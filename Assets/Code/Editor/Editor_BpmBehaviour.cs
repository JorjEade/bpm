﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(BpmDetectorBehaviour))]
public class Editor_BpmDetectorBehaviour : Editor_DetectorBehaviour<BpmDetector> { }


[CustomEditor(typeof(DownbeatDetectorBehaviour))]
public class Editor_DownbeatDetectorBehaviour : Editor_DetectorBehaviour<DownbeatDetector> { }


public class Editor_DetectorBehaviour<TDetector> : Editor where TDetector : Detector, new()
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		DetectorBehaviour<TDetector> targ = (DetectorBehaviour<TDetector>)target;

		GUILayout.BeginHorizontal();
		{
			if (GUILayout.Button("Load From Clipboard"))
			{
				if (!string.IsNullOrEmpty(EditorGUIUtility.systemCopyBuffer))
					targ.Detect(new Track() { path = EditorGUIUtility.systemCopyBuffer.Replace("&amp;", "&") });
			}

			if (GUILayout.Button("Open"))
			{
				string path = EditorUtility.OpenFilePanel("Open", "", "*.*");

				if (!string.IsNullOrEmpty(path))
					targ.Detect(new Track() { path = path });
			}
		}
		GUILayout.EndHorizontal();
	}
}
