﻿#pragma warning disable 0162

using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;


public class BpmDetector_Coarse : IBpmDetector
{
	const bool LOG_WARNINGS = false;

	private const int numThreads = 20;
	private readonly Action[] threadActions = new Action[numThreads];


	// Wave
	private const int sampleRate = 5000;
	private const float peakAccel = 5;
	private const float lpCutoffFreq = 15; //12.7679971354f;
	private const int lpOrder = 1;
	private static float[] wave = new float[sampleRate * 60 * 10];

	// Weights
	private const float frameLengthBars = 4;
	private const float frameIncrTime = 0.1f;

	// Weights smoothing
	private const int smoothingValuesEitherSide = 10;
	private const int smoothingPasses = 2;

	// Peak detection
	private const float lookAheadSpb = 0.035f;

	// Valid peak detection
	private const float minAllowedPeakDist = 0.03f;
	private const float minAllowedPeakHightPercentOfMax = 0.65f;
	private const float maxSkew = 0.6f;

	private static readonly int[] emptyIntArray = new int[0];
	private static readonly float[] emptyFloatArray = new float[0];
	private static float[] spbs = new float[1201];
	private static float[] weights = new float[1201];



	decimal IBpmDetector.Detect(Track track, decimal[] bpms, bool isFinalPass, out float[] weightsOut, out int[] peakTroughIndexesOut, out int[] validPeaksOut)
	{
		peakTroughIndexesOut = emptyIntArray;
		validPeaksOut = emptyIntArray;
		weightsOut = emptyFloatArray;

		decimal bpm = 0;
		ArrayUtils.EnsureArrayLength(ref spbs, bpms.Length);
		float spbMin = AudioUtils.SecondsPerBar(bpms.Last());
		float spbMax = AudioUtils.SecondsPerBar(bpms.First());
		for (int i = 0; i < spbs.Length; ++i)
			spbs[i] = Mathf.LerpUnclamped(spbMin, spbMax, i / (float)spbs.Length);


		// Get start/end time
		float start = 0;
		float end = float.MaxValue;
		AudioUtils.GetStartAndEndOfNoise(track.Clip, out start, out end);


		// Get wave array
		int waveLength = track.Clip.GetWaveArray(ref wave, sampleRate, start, end, peakAccel, lpCutoffFreq, lpOrder);
		if (waveLength == -1)
			return Detector.INVALID_VALUE;


		// Get weights
		if (ArrayUtils.EnsureArrayLength(ref weights, bpms.Length))
			Array.Clear(weights, 0, weights.Length);
		weightsOut = weights;

		int bpmsPerThread = Mathf.CeilToInt(spbs.Length / (float)numThreads);

		for (int threadIndex = 0; threadIndex < numThreads; ++threadIndex)
		{
			int bpmsIndexMin = threadIndex * bpmsPerThread;
			int bpmsIndexMaxExclusive = Mathf.Min((threadIndex + 1) * bpmsPerThread, spbs.Length);
			const int indexIncrement = (int)(frameIncrTime * sampleRate);

			threadActions[threadIndex] = () => {
				for (int i = bpmsIndexMin; i < bpmsIndexMaxExclusive; ++i)
				{
					int frameLength = AudioUtils.SamplesInBars(frameLengthBars, AudioUtils.SpbToBpm(spbs[i]), sampleRate);
					weights[i] = -wave.GetAverageOfSuccessiveSqDiffs(waveLength, frameLength, indexIncrement);
				}
			};
		}
		ThreadUtils.RunActionsThreaded(threadActions);


		// Clean up weights distribution
		int weightsTrim = smoothingValuesEitherSide * smoothingPasses;
		weights.Smooth(smoothingValuesEitherSide, smoothingPasses);
		weights.NormaliseRange();

		
		// Find weight peaks
		List<int> peakIndexes = new List<int>();
		List<float> peakHeights = new List<float>();
		List<int> peakTroughIndexes = new List<int>();
		List<float> peakTroughWeights = new List<float>();
		float lastNonZeroDiff = 0;
		bool hasAddedTrough = false;
		
		int lookAhead = Mathf.RoundToInt((spbs.Length - 1) * (lookAheadSpb / spbs.Range()));
		int iMax = spbs.Length - lookAhead - weightsTrim - 1;

		for (int i = weightsTrim; i <= iMax; ++i)
		{
			float diff = weights[i + lookAhead] - weights[i];

			// Force the final iteration to add a peak/trough
			if (i == iMax) diff *= -1;

			if (diff != 0)
			{
				if (Mathf.Sign(diff) != Mathf.Sign(lastNonZeroDiff) || lastNonZeroDiff == 0)
				{
					bool isTrough = lastNonZeroDiff <= 0 && diff > 0;

					// Ensure a trough is added before any peaks
					if (isTrough || hasAddedTrough)
					{
						int peakTroughIndex = i + lookAhead / 2;
						peakTroughIndex = weights.FindPeakOrTrough(peakTroughIndex, !isTrough);
						peakTroughIndexes.Add(peakTroughIndex);
						peakTroughWeights.Add(weights[peakTroughIndex]);
						hasAddedTrough = true;

						// If this is not the first trough, add the preceding peak
						if (isTrough && peakTroughIndexes.Count > 1)
						{
							peakIndexes.Add(peakTroughIndexes.SecondToLast());
							float peakBase = (peakTroughWeights.ThirdToLast() + peakTroughWeights.Last()) / 2;
							float peakHeight = peakTroughWeights.SecondToLast() - peakBase;
							peakHeights.Add(peakHeight);
						}
					}
				}
				lastNonZeroDiff = diff;
			}
		}

		// If no peaks, return invalid
		if (peakTroughIndexes.Count == 0)
		{
			Debug.LogError("No peaks!");
			return Detector.INVALID_VALUE;
		}

		peakTroughIndexesOut = new int[peakTroughIndexes.Count];
		peakTroughIndexes.CopyTo(peakTroughIndexesOut);


		// Find valid peaks
		float currentMinAllowedPeakHightPercentOfMax = minAllowedPeakHightPercentOfMax;

	RefindValidPeaks:
		float peakHeightMax = peakHeights.Max();
		
		List<int> validPeakIndexes = new List<int>();
		List<float> validPeakSpbs = new List<float>();

		for (int i = 0; i < peakIndexes.Count; ++i)
		{
			// Check if peak is high enough
			float percentOfMax = peakHeights[i] / peakHeightMax;
			if (percentOfMax < currentMinAllowedPeakHightPercentOfMax)
				continue;

			// Check if peak is too skewed
			int indexInPeaksAndTroughs = i * 2 + 1;
			float spbOfPrevTrough =	spbs[peakTroughIndexes[indexInPeaksAndTroughs - 1]];
			float spbOfCurrPeak =	spbs[peakTroughIndexes[indexInPeaksAndTroughs]];
			float spbOfNextTrough =	spbs[peakTroughIndexes[indexInPeaksAndTroughs + 1]];
			float skew = Mathf.Lerp(-1, 1, Mathf.InverseLerp(spbOfPrevTrough, spbOfNextTrough, spbOfCurrPeak));

			if (Mathf.Abs(skew) > maxSkew)
			{
				if (LOG_WARNINGS)
					Debug.LogWarningFormat("Peak at {0} is too skewed: {1}", spbOfCurrPeak, skew);
				continue;
			}

			validPeakIndexes.Add(peakIndexes[i]);
			validPeakSpbs.Add(spbs[peakIndexes[i]]);

			//Debug.LogFormat("Valid peak: {0} ({1:0%} of max) {2}", AudioUtils.SpbToBpm(spbs[peakIndexes[i]]), percentOfMax, spbs[peakIndexes[i]]);
		}

		// If no valid peaks, return max peak
		if (validPeakIndexes.Count == 0)
		{
			Debug.LogError("No valid peaks! Returning max peak");
			bpm = AudioUtils.SpbToBpm(spbs[peakIndexes[peakHeights.GetIndexOfMax()]]);
			goto ClimbPeakAtBPM;
		}

		validPeaksOut = new int[validPeakIndexes.Count];
		validPeakIndexes.CopyTo(validPeaksOut);

		// If only one valid peak, return its BPM
		if (validPeakIndexes.Count == 1) {
			bpm = AudioUtils.SpbToBpm(validPeakSpbs[0]);
			goto ClimbPeakAtBPM;
		}


		// Find min peak distance
		float minPeakDist = validPeakSpbs.GetGcdOfIntervals();

		if (minPeakDist == 0)
		{
			if (currentMinAllowedPeakHightPercentOfMax >= 1)
			{
				Debug.LogError("Unable to find GCD and already at max threshold");
				return Detector.INVALID_VALUE;
			}
			else
			{
				if (LOG_WARNINGS)
					Debug.LogWarningFormat("Unable to find GCD. Increasing threshold from {0} to {1}", currentMinAllowedPeakHightPercentOfMax, currentMinAllowedPeakHightPercentOfMax + 0.01f);

				currentMinAllowedPeakHightPercentOfMax += 0.01f;
				goto RefindValidPeaks;
			}
		}
		

		// Get BPM
		float bpmExp = AudioUtils.GetBpmExponent(AudioUtils.SpbToBpm(minPeakDist));
		float bpmPo2 = Mathf.Pow(2, bpmExp);
		bpm = (decimal)MathUtils.WrapLogarithmic(bpmPo2, 95);


	ClimbPeakAtBPM:

		// Climb to peak at that BPM
		float spb = AudioUtils.SecondsPerBar(bpm);
		int spbIndex = Mathf.RoundToInt((spbs.Length - 1) * (Mathf.InverseLerp(spbs[0], spbs[spbs.Length - 1], spb)));
		spbIndex = weights.FindPeakOrTrough(spbIndex, true);
		bpm = decimal.Round(AudioUtils.SpbToBpm(spbs[spbIndex]), 1);

		return bpm;
	}
}
