﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;


public class BpmDetector : Detector
{
	
	public const decimal bpmMin = 80;
	public const decimal bpmMax = 200;
	public const int maxDP = 3;

	private readonly IBpmDetector detectorCoarse = new BpmDetector_Coarse();
	private readonly IBpmDetector detectorFine = new BpmDetector_Fine();

	
	
	public override decimal ActualValue
	{
		get { return Track != null ? Track.bpm : INVALID_VALUE; }
	}


	public override bool CloseEnough
	{
		get
		{
			const decimal tolerance = 0.8m;
			return
				(Math.Abs(ActualValue - DetectedValue) < tolerance) ||
				(Math.Abs(ActualValue - DetectedValue * 2) < tolerance) ||
				(Math.Abs(ActualValue - DetectedValue / 2) < tolerance);
		}
	}


	protected override IEnumerator DetectAbstract()
	{
		decimal[] bpms;

		for (int po10 = 0; po10 >= -maxDP; --po10)
		{
			IBpmDetector detector;

			switch (po10)
			{
				case 0:
					bpms = AudioUtils.GetBpmRange(bpmMin, bpmMax, 0.1m);
					detector = detectorCoarse;
					break;
					
				default:
					decimal interval = (decimal)Mathf.Pow(10, po10);
					bpms = AudioUtils.GetBpmRange(DetectedValue - interval * 5, DetectedValue + interval * 5, interval);
					detector = detectorFine;
					break;
			}


			/////////////// SWEEP ///////////////
			SWEEP:
			yield return null;
			Debug.LogFormat("   Sweep: [{0} ... {1}] incr: {2}", bpms.First(), bpms.Last(), bpms.Length > 1 ? bpms[1] - bpms[0] : 0);
			DateTime t = DateTime.Now;
			float[] weights;
			int[] peakTroughIndexes;
			int[] validPeakIndexes;
			DetectedValue = detector.Detect(Track, bpms, po10 == -maxDP, out weights, out peakTroughIndexes, out validPeakIndexes);
			Debug.LogFormat("   ...Sweep - BPM: <b>{0}</b> Time: <b>{1:0.000}</b>", DetectedValue, (DateTime.Now - t).TotalSeconds);

			Texture2D debugTex = AddDebugTex();
			if (po10 == 0)
			{
				DrawWeightDistribution(
					debugTex,
					weights,
					AudioUtils.SecondsPerBar(bpms.Last()), AudioUtils.SecondsPerBar(bpms.First()),
					AudioUtils.SecondsPerBar(ActualValue), AudioUtils.SecondsPerBar(ActualValue / 2), AudioUtils.SecondsPerBar(ActualValue * 2),
					AudioUtils.SecondsPerBar(DetectedValue), AudioUtils.SecondsPerBar(DetectedValue / 2), AudioUtils.SecondsPerBar(DetectedValue * 2),
					peakTroughIndexes, validPeakIndexes);
			}
			else
			{
				DrawWeightDistribution(
					debugTex,
					weights,
					(float)bpms.First(), (float)bpms.Last(),
					(float)ActualValue, (float)ActualValue / 2, (float)ActualValue * 2,
					(float)DetectedValue, (float)DetectedValue / 2, (float)DetectedValue * 2,
					peakTroughIndexes, validPeakIndexes
				);
			}

			if (DetectedValue == INVALID_VALUE)
				break;

			/////////////////////////////////////


			switch (po10)
			{
				case 0:
					break;

				default:
					if (DetectedValue == bpms.First())
					{
						Debug.LogError("   ...BPM was minimum - shifting range down");
						bpms.OffsetValues(bpms.First() - bpms.Last() + (decimal)Mathf.Pow(10, po10));
						goto SWEEP;
					}
					if (DetectedValue == bpms.Last())
					{
						Debug.LogError("   ...BPM was maximum - shifting range up");
						bpms.OffsetValues(bpms.Last() - bpms.First() - (decimal)Mathf.Pow(10, po10));
						goto SWEEP;
					}
					break;
			}
		}

		yield break;
	}



	protected void DrawWeightDistribution(
		Texture2D tex,
		float[] weights,
		float xMin, float xMax,
		float xTarget = -1, float xTargetHalf = -1, float xTargetDouble = -1,
		float xDetected = -1, float xDetectedHalf = -1, float xDetectedDouble = -1,
		int[] peakTroughIndexes = null,
		int[] validPeakIndexes = null,
		bool asd = false)
	{
		tex.Fill(new Color(1, 1, 1, 0));

		Func<float, int> GetTexXFromX = (float x) =>
			(int)(tex.width * MathUtils.InverseLerpUnclamped(xMin, xMax, x));

		Func<int, int> GetTexXFromIndex = (int index) =>
			GetTexXFromX(Mathf.LerpUnclamped(xMin, xMax, index / (float)(weights.Length - 1)));
		
		Func<float, int> GetTexYFromWeight = (float weight) =>
			(int)(tex.height * Mathf.Lerp(0.1f, 0.9f, weight));


		// Weights
		for (int i = 1; i < weights.Length; ++i)
		{
			tex.DrawLine(
				GetTexXFromIndex(i - 1), GetTexYFromWeight(weights[i - 1]),
				GetTexXFromIndex(i - 0), GetTexYFromWeight(weights[i - 0]),
				Color.grey
			);
		}


		// Peaks and troughs
		if (peakTroughIndexes != null)
		{
			for (int i = 0; i < peakTroughIndexes.Length; ++i)
			{
				int x = GetTexXFromIndex(peakTroughIndexes[i]);
				int y = GetTexYFromWeight(weights[peakTroughIndexes[i]]);
				bool isTrough = i == 0 || weights[peakTroughIndexes[i]] < weights[peakTroughIndexes[i - 1]];
				tex.DrawLineV(y, y - 10, x, isTrough ? Color.red : Color.green, 1);
			}
		}


		// Valid peaks
		if (validPeakIndexes != null)
		{
			for (int i = 0; i < validPeakIndexes.Length; ++i)
			{
				int x = GetTexXFromIndex(validPeakIndexes[i]);
				int y = GetTexYFromWeight(weights[validPeakIndexes[i]]);
				tex.DrawLineV(y, y - 100, x, Color.magenta, 3);
			}
		}


		// Markers: target
		int lineTopTarget = (int)(tex.height * 1.00f);
		int lineBotTarget = (int)(tex.height * (asd ? 0 : 0.95f));
		if (xTargetHalf != -1)		tex.DrawLineV(lineTopTarget, lineBotTarget, GetTexXFromX(xTargetHalf),		new Color(0, 1, 0, 0.3f),	thickness: 3);
		if (xTarget != -1)			tex.DrawLineV(lineTopTarget, lineBotTarget, GetTexXFromX(xTarget),			Color.green,				thickness: asd ? 1 : 3);
		if (xTargetDouble != -1)	tex.DrawLineV(lineTopTarget, lineBotTarget, GetTexXFromX(xTargetDouble),	new Color(0, 1, 0, 0.3f),	thickness: 3);
		

		// Markers: detected
		int lineTopDetect = (int)(tex.height * 0.95f);
		int lineBotDetect = (int)(tex.height * 0.90f);
		if (xDetectedHalf != -1)	tex.DrawLineV(lineTopDetect, lineBotDetect, GetTexXFromX(xDetectedHalf),	new Color(1, 0, 0, 0.3f),	thickness: 3);
		if (xDetected != -1)		tex.DrawLineV(lineTopDetect, lineBotDetect, GetTexXFromX(xDetected),		Color.red,					thickness: 3);
		if (xDetectedDouble != -1)	tex.DrawLineV(lineTopDetect, lineBotDetect, GetTexXFromX(xDetectedDouble),	new Color(1, 0, 0, 0.3f),	thickness: 3);


		// Done
		tex.Apply();
	}

}
