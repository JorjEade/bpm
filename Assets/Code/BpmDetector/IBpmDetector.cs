﻿using UnityEngine;

public interface IBpmDetector
{

	decimal Detect(Track track, decimal[] bpms, bool isFinalPass, out float[] weights, out int[] peakIndexesOut, out int[] validPeaksOut);

}
