﻿using System;
using System.Linq;
using UnityEngine;



public class BpmDetector_Fine : IBpmDetector
{

	private const float lookBackBarsDefault = 8;
	private const int minDivisions = 8;
	private const float deltaMs = 1;
	private const float deltaMsFinal = 0.1f;
	private const float samplePeakDecayAccel = 0.1f;
	private const int numThreads = 20;
	private readonly Action[] threadActions = new Action[numThreads];
	private const float blockPrewarmMs = 2;

	private AudioClip clip;
	private int channels;
	private int frequency;
	private float[] block;
	private float[] ringBuffer;
	private int maxSampleIndexInRingBuffer;
	private float samplePeakAccel;
	private static float[] weights = new float[11];
	private static int[] numSamplesToLookBackForBpms = new int[11];
	private int blockLengthSamples;
	private int blockPrewarmSamples;
	private int deltaSample;
	private int blockLengthSamplesPerThread;
	private int clipSamplesRoundedToBlockSamples;
	private int clipStartSample;
	private int clipNumSamplesTrimmed;
	private static readonly int[] emptyIntArray = new int[0];



	private void SetClip(AudioClip clip)
	{
		this.clip = clip;
		channels = clip.channels;
		frequency = clip.frequency;

		float start, end;
		AudioUtils.GetStartAndEndOfNoise(clip, out start, out end);
		clipStartSample = (int)(start * frequency);
		clipNumSamplesTrimmed = (int)((end - start) * frequency);

		blockPrewarmSamples = (int)(frequency * blockPrewarmMs / 1000);
		samplePeakAccel = -samplePeakDecayAccel / frequency;
	}



	decimal IBpmDetector.Detect(Track track, decimal[] bpms, bool isFinalPass, out float[] weightsOut, out int[] peakIndexesOut, out int[] validPeaksOut)
	{
		peakIndexesOut = emptyIntArray;
		validPeaksOut = emptyIntArray;

		// Handle clip changed
		if (track.Clip != clip)
			SetClip(track.Clip);

		// Handle array length changed
		ArrayUtils.EnsureArrayLength(ref numSamplesToLookBackForBpms, bpms.Length);
		if (ArrayUtils.EnsureArrayLength(ref weights, bpms.Length))
			Array.Clear(weights, 0, weights.Length);
		weightsOut = weights;


		deltaSample = (int)((isFinalPass ? deltaMsFinal : deltaMs) / 1000 * frequency);
		maxSampleIndexInRingBuffer = 0;
		int lookBackBarsMax = AudioUtils.GetNumBars(clipNumSamplesTrimmed, frequency, bpms.First()) / minDivisions;
		float lookBackBars = Mathf.Min(lookBackBarsDefault, lookBackBarsMax);


	CREATE_BUFFERS:

		blockLengthSamples = MathUtils.RoundUpToNearest(AudioUtils.SamplesInBars(lookBackBars, BpmDetector.bpmMin - 1, frequency), deltaSample);
		block = new float[blockLengthSamples * channels];
		ringBuffer = new float[blockLengthSamples * 2];
		blockLengthSamplesPerThread = Mathf.CeilToInt(blockLengthSamples / (float)numThreads);
		clipSamplesRoundedToBlockSamples = MathUtils.RoundDownToNearest(clipNumSamplesTrimmed, blockLengthSamples);

		// Populate numSamplesToLookBackForBpms
		for (int i = 0; i < bpms.Length; ++i)
		{
			numSamplesToLookBackForBpms[i] = AudioUtils.SamplesInBars(lookBackBars, bpms[i], frequency);

			if (i > 0 && numSamplesToLookBackForBpms[i - 1] == numSamplesToLookBackForBpms[i])
			{
				Debug.LogErrorFormat(
					"BPMs {0} and {1} share the same lookback sample length: {2}. Doubling lookBackBars from {3} to {4}...",
					bpms[i - 1], bpms[i], numSamplesToLookBackForBpms[i], lookBackBars, lookBackBars * 2
				);

				lookBackBars *= 2;

				if (lookBackBars > lookBackBarsMax)
				{
					Debug.LogErrorFormat("...Exceded max lookback bars ({0}). Returning middle bpm ({1})", lookBackBarsMax, bpms[bpms.Length / 2]);
					return bpms[bpms.Length / 2];
				}
				else
				{
					goto CREATE_BUFFERS;
				}
			}
		}


		// Process blocks
		LoadNextBlockIntoRingBuffer();

		for (int currSampleIndex = blockLengthSamples; currSampleIndex < clipSamplesRoundedToBlockSamples - blockLengthSamples; currSampleIndex += blockLengthSamples)
		{
			LoadNextBlockIntoRingBuffer();
			ProcessBlock(currSampleIndex);
		}

		weights.NormaliseRange();

		return bpms[weights.GetIndexOfMax()];
	}



	// ------------------------------- RING BUFFER -------------------------------

	private void LoadNextBlockIntoRingBuffer()
	{
		clip.GetData(block, clipStartSample + maxSampleIndexInRingBuffer);
		
		for (int threadIndex = 0; threadIndex < numThreads; ++threadIndex)
		{
			int blockSampleStartIndex = threadIndex * blockLengthSamplesPerThread;
			int blockSampleEndIndexExclusive = Mathf.Min((threadIndex + 1) * blockLengthSamplesPerThread, blockLengthSamples);
			threadActions[threadIndex] = () => LoadNextBlockIntoRingBuffer_SplitBetweenThreads(blockSampleStartIndex, blockSampleEndIndexExclusive);
		}

		ThreadUtils.RunActionsThreaded(threadActions);

		maxSampleIndexInRingBuffer += blockLengthSamples;
	}



	private void LoadNextBlockIntoRingBuffer_SplitBetweenThreads(int blockSampleIndexStart, int blockSampleIndexEndExclusive)
	{
		float samplePeak = 0;
		float samplePeakVel = 0;
		
		for (int blockSampleIndex = Mathf.Max(blockSampleIndexStart - blockPrewarmSamples, 0); blockSampleIndex < blockSampleIndexEndExclusive; ++blockSampleIndex)
		{
			float sample = block[blockSampleIndex * channels];
			float sampleSq = sample * sample;

			if (sampleSq > samplePeak * samplePeak)
			{
				samplePeak = Mathf.Abs(sample);
				samplePeakVel = 0;
			}
			else
			{
				samplePeakVel += samplePeakAccel;
				samplePeak += samplePeakVel;
				if (samplePeak < 0) samplePeak = 0;
			}

			if (blockSampleIndex >= blockSampleIndexStart)
			{
				ringBuffer[(maxSampleIndexInRingBuffer + blockSampleIndex) % ringBuffer.Length] = samplePeak;
			}
		}
	}



	// ------------------------------- SAMPLE PROCESSING -------------------------------

	private void ProcessBlock(int blockStartSampleIndex)
	{
		for (int threadIndex = 0; threadIndex < numThreads; ++threadIndex)
		{
			int sampleIndexMin = blockStartSampleIndex + threadIndex * blockLengthSamplesPerThread;
			int sampleIndexMaxExclusive = Mathf.Min(blockStartSampleIndex + (threadIndex + 1) * blockLengthSamplesPerThread, clipSamplesRoundedToBlockSamples);
			threadActions[threadIndex] = () => ProcessBlock_SplitBetweenThreads(sampleIndexMin, sampleIndexMaxExclusive);
		}

		ThreadUtils.RunActionsThreaded(threadActions);
	}



	private void ProcessBlock_SplitBetweenThreads(int sampleIndexMin, int sampleIndexMaxExclusive)
	{
		float[] weights = new float[numSamplesToLookBackForBpms.Length];

		for (int sampleIndex = sampleIndexMin; sampleIndex < sampleIndexMaxExclusive; sampleIndex += deltaSample)
		{
			int ringBufferIndex = sampleIndex % ringBuffer.Length;
			float sampleCurr = ringBuffer[ringBufferIndex];

			for (int i = numSamplesToLookBackForBpms.Length - 1; i >= 0; --i)
			{
				ringBufferIndex = (sampleIndex - numSamplesToLookBackForBpms[i]) % ringBuffer.Length;
				float diff = ringBuffer[ringBufferIndex] - sampleCurr;
				weights[i] -= diff * diff;
			}
		}

		lock (BpmDetector_Fine.weights)
		{
			for (int i = weights.Length - 1; i >= 0; --i)
				BpmDetector_Fine.weights[i] += weights[i];
		}
	}

}

