﻿using UnityEngine;


public class BpmDetectorBehaviour : DetectorBehaviour<BpmDetector>
{

	protected override void OnGUI()
	{
		base.OnGUI();

		if (detectors.Count > 0)
		{
			// Value at mouse x
			float normMouseX = Input.mousePosition.x / Screen.width;
			float spb = Mathf.LerpUnclamped(AudioUtils.SecondsPerBar(BpmDetector.bpmMax), AudioUtils.SecondsPerBar(BpmDetector.bpmMin), normMouseX);
			decimal bpm = AudioUtils.SpbToBpm(spb);
			GUI.Label(
				new Rect(Screen.width - 100, 0, 100, 100),
				string.Format("{0:0.00}, {1:0.0}", spb, bpm)
			);
		}
	}

}
