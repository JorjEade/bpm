﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Xml : MonoBehaviour
{

	public enum Mode
	{
		Linear,
		Random
	}

	private const string path = @"C:\Users\George\AppData\Roaming\Mixer\Local Store\save data.xml";
	private const int numTracksAheadToLoad = 2;
	private static readonly Color logCol = new Color(0.5f, 0.5f, 0);
	private readonly List<Track> tracks = new List<Track>();

	public Mode mode = Mode.Linear;
	public bool onlyOne;
	public string[] firstTracks;
	public bool stopAfterFirstTracks;

	[Header("Continue...")]
	public float delayOnDetected;
	public bool waitForKeyPress;

	[Header("On detected...")]
	public bool pauseOnDetected;
	public bool playClipOnDetected;



	private IEnumerator Start()
	{
		Parse();

		// Set the detector
		DownbeatDetectorBehaviour detector = gameObject.AddComponent<DownbeatDetectorBehaviour>();
		//BpmDetectorBehaviour detector = gameObject.AddComponent<BpmDetectorBehaviour>();


		int firstTracksIndex = 0;
		int currentTrackIndex = 0;

		// Detect tracks
		while (true)
		{
			if (firstTracksIndex < firstTracks.Length)
			{
				currentTrackIndex = tracks.FindIndex(t => t.name.ToLower().Contains(firstTracks[firstTracksIndex].ToLower()));

				if (currentTrackIndex == -1) {
					DebugUtils.LogError(logCol, "Track \"{0}\" not found", firstTracks);
					yield break;
				}

				++firstTracksIndex;
			}
			else
			{
				if (stopAfterFirstTracks)
					yield break;
			
				switch (mode)
				{
					case Mode.Linear:

						++currentTrackIndex;

						// Check end condition
						if (currentTrackIndex == tracks.Count - 1)
							yield break;

						// Ensure next few tracks are loading
						if (!stopAfterFirstTracks)
							for (int i = 1; i <= numTracksAheadToLoad; ++i)
								if (currentTrackIndex < tracks.Count - i && tracks[currentTrackIndex + i].LoadingState == AudioDataLoadState.Unloaded)
									StartCoroutine(tracks[currentTrackIndex + i].LoadClip());
						break;

					case Mode.Random:
						currentTrackIndex = Random.Range(0, tracks.Count);
						break;
				}
			}


			// Detect
			DebugUtils.Log(logCol, "XML: Detect track {0}", currentTrackIndex);
			detector.Clear();
			detector.playClipOnDetected = playClipOnDetected;
			yield return detector._Detect(tracks[currentTrackIndex]);

			if (!enabled || onlyOne)
				yield break;

			if (delayOnDetected > 0)
				yield return new WaitForSeconds(delayOnDetected);

			if (waitForKeyPress)
				while (!Input.GetKeyDown(KeyCode.Space))
					yield return null;

#if UNITY_EDITOR
			if (pauseOnDetected) {
				UnityEditor.EditorApplication.isPaused = true;
				yield return null;
			}
#endif
		}
	}


	private void Parse()
	{
		using (XmlReader reader = XmlReader.Create(path))
		{
			while (reader.ReadToFollowing("track"))
			{
				Track track = new Track();
				track.index = tracks.Count;

				track.path = reader["url"];
					
				if (reader.ReadToFollowing("title"))
				{
					reader.Read();
					track.name = reader.Value;
				}

				if (reader.ReadToFollowing("bpm"))
				{
					reader.Read();
					track.bpm = decimal.Round(decimal.Parse(reader.Value), 3);
				}

				if (reader.ReadToFollowing("downbeatSample"))
				{
					reader.Read();
					track.downbeatSample = int.Parse(reader.Value) % AudioUtils.SamplesInBars(1, track.bpm, 44100);
					track.downbeatTime = (decimal)(track.downbeatSample / 44100f);
				}

				tracks.Add(track);
			}
		}
	}

}
