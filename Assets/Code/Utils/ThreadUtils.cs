﻿using System;
using System.Threading;
using System.Collections.Generic;

public static class ThreadUtils
{


	public static void RunActionsThreaded(params Action[] actions)
	{
		RunActionsThreaded((IList<Action>)actions);
	}


	public static void RunActionsThreaded(IList<Action> actions)
	{
		var handles = new ManualResetEvent[actions.Count];
		for (var i = 0; i < actions.Count; i++)
		{
			handles[i] = new ManualResetEvent(false);
			var currentAction = actions[i];
			var currentHandle = handles[i];
			Action wrappedAction = () => { try { currentAction(); } finally { currentHandle.Set(); } };
			ThreadPool.QueueUserWorkItem(x => wrappedAction());
		}

		WaitHandle.WaitAll(handles);
	}

}
