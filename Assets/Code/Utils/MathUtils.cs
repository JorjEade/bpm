using System;
using System.Collections.Generic;
using UnityEngine;



public static class MathUtils
{

	public const float TAU = Mathf.PI * 2;
	private const float LOG2 = 0.693147180559945f; //(float)Math.Log(2);
	


	public static double Wrap(double val, double closedMin, double openMax) {
		return Wrap(val - closedMin, openMax - closedMin) + closedMin;
	}
	public static float Wrap(float val, float closedMin, float openMax) {
		return Wrap(val - closedMin, openMax - closedMin) + closedMin;
	}
	public static int Wrap(int val, int closedMin, int openMax) {
		return Wrap(val - closedMin, openMax - closedMin) + closedMin;
	}



	///<summary>Wraps between [0, openMax)</summary>
	public static int Wrap(int val, int openMax) {
		return ((val % openMax) + openMax) % openMax;
	}

	///<summary>Wraps between [0, openMax)</summary>
	public static float Wrap(float val, float openMax)
	{
		if (float.IsInfinity(openMax)) {
			return val;
		}
		val %= openMax;
		return val >= 0 ? val : val + openMax;
	}

	///<summary>Wraps between [0, openMax)</summary>
	public static double Wrap(double val, double openMax)
	{
		if (openMax == double.PositiveInfinity || openMax == double.NegativeInfinity) {
			return val;
		}
		val %= openMax;
		return val >= 0 ? val : val + openMax;
	}



	public static float Wrap01(float val)
	{
		return Wrap(val, 1);
	}



	public static bool IsInOpenRange(float val, float min, float max)
	{
		return val > min && val < max;
	}



	public static bool IsInClosedRange(float val, float min, float max)
	{
		return val >= min && val <= max;
	}
	public static bool IsInClosedRange(double val, double min, double max)
	{
		return val >= min && val <= max;
	}



	public static bool IsBetween01(float val)
	{
		return IsInOpenRange(val, 0, 1);
	}



	public static float Log2(float e)
	{
		return (float)Math.Log(e) / LOG2;
	}



	public static float Loop(float val, float start, float end)
	{
		if (val < start) {
			return val;
		}

		return Wrap(val, start, end);
	}



	public static float PingPong(float val, float start, float end)
	{
		if (val < start) {
			return val;
		}

		return Mathf.PingPong(val - start, end - start) + start;
	}



	public static double Round(double val, double to = 1)
	{
		if (to == 0) {
			return val;
		}
		if (to == float.PositiveInfinity) {
			return 0;
		}
		return Math.Round(val / to) * to;
	}



	public static float Round(float val, float to = 1)
	{
		if (to == 0) {
			return val;
		}
		if (to == float.PositiveInfinity) {
			return 0;
		}
		return Mathf.Round(val / to) * to;
	}



	public static double Floor(double val, double to = 1)
	{
		if (to == 0) {
			return val;
		}
		if (to == double.PositiveInfinity) {
			return 0;
		}
		return Math.Floor(val / to) * to;
	}



	public static double Ceil(double val, double to = 1)
	{
		if (to == 0) {
			return val;
		}
		return Math.Ceiling(val / to) * to;
	}



	public static bool IsEven(int num)
	{
		return (int)(num / 2) == (num / 2);
	}


	public static double Clamp(double val, double min = double.NegativeInfinity, double max = double.PositiveInfinity)
	{
		return Math.Min(Math.Max(val, min), max);
	}


	/// <summary>
	/// Returns 0 if a or b is infinite and the other is zero
	/// </summary>
	public static double Multiply(double a, double b)
	{
		if ((double.IsInfinity(a) && b == 0) ||
			(double.IsInfinity(b) && a == 0)
		) {
			return 0;
		}

		return a * b;
	}



	public static float Square(float val)
	{
		return val * val;
	}


	public static double Square(double val)
	{
		return val * val;
	}


	public static float InverseLerpUnclamped(float min, float max, float val)
	{
		return (val - min) / (max - min);
	}


	public static float WrapLogarithmic(float val, float min, float @base = 2)
	{
		while (val < min) val *= @base;
		while (val >= min * @base) val /= @base;
		return val;
	}


	public static float DistFromNearestInteger(float val)
	{
		return Mathf.Abs(Wrap(val, -0.5f, 0.5f));
	}


	public static float RoundToNearest(float val, float nearest)
	{
		return nearest * (Mathf.Round(val / nearest));
	}


	public static int RoundUpToNearest(int val, int nearest)
	{
		return Mathf.CeilToInt(val / (float)nearest) * nearest;
	}


	public static int RoundDownToNearest(int val, int nearest)
	{
		return (val / nearest) * nearest;
	}


	public static float RoundDownToNearest(float val, float nearest)
	{
		return (int)(val / nearest) * nearest;
	}


	public static int SignOrZero(float targ)
	{
		return targ > 0 ? 1 : targ < 0 ? -1 : 0;
	}


	public static float Pow5(float n)
	{
		return n * n * n * n * n;
	}


	public static float GetAngleDifferenceRad(float a, float b)
	{
		return GetCyclicDifference(a, b, TAU);
	}


	public static float GetAngleDifferenceDeg(float a, float b)
	{
		return GetCyclicDifference(a, b, 360);
	}


	public static float GetCyclicDifference(float a, float b, float period)
	{
		return Wrap(b - a, -period / 2, period / 2);
	}


}
