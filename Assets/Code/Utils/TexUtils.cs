﻿using System;
using UnityEngine;



public static class TexUtils
{

	

	public static Texture2D GetOnePixelTex(Color color)
	{
		Texture2D ret = new Texture2D(1, 1);
		ret.SetPixel(0, 0, color);
		ret.Apply();

		return ret;
	}



	public static Texture2D GetRectTex(int width, int height, Color? col = null, bool doApply = false)
	{
		Texture2D ret = new Texture2D(width, height);
		Fill(ret, col.HasValue ? col.Value : Color.clear);
		ret.wrapMode = TextureWrapMode.Clamp;
		if (doApply) ret.Apply();
		return ret;
	}



	public static Texture2D GetRectTex(Texture2D tex, Color bgCol, Color borderCol, int borderThicknessH, int borderThicknessV, bool doApply = false)
	{
		Color[] colours = new Color[tex.width * tex.height];

		for (int x = 0; x < tex.width; ++x)
		{
			for (int y = 0; y < tex.height; ++y)
			{
				colours[x + tex.width * y] =
					(x < borderThicknessV || x >= tex.width - borderThicknessV || y < borderThicknessH || y >= tex.height - borderThicknessH) ?
					borderCol :
					bgCol;
			}
		}

		tex.SetPixels(colours);

		tex.wrapMode = TextureWrapMode.Clamp;

		if (doApply) {
			tex.Apply();
		}

		return tex;
	}



	public static void DrawLine(this Texture2D tex, float xStart, float yStart, float xEnd, float yEnd, Color col, bool doApply = false)
	{
		float res = 1 / Math.Max(Mathf.Abs(xStart - xEnd), Mathf.Abs(yStart - yEnd));
		
		for (float t = 0; t <= 1; t += res)
		{
			tex.SetPixel(
				Mathf.Clamp(Mathf.RoundToInt(Mathf.Lerp(xStart, xEnd, t)), 0, tex.width - 1),
				Mathf.Clamp(Mathf.RoundToInt(Mathf.Lerp(yStart, yEnd, t)), 0, tex.height - 1),
				col
			);
		}

		if (doApply) {
			tex.Apply();
		}
	}



	public static void DrawLineH(this Texture2D tex, float y, Color col, int thickness = 1, bool doApply = false)
	{
		DrawLineH(tex, 0, tex.width, y, col, thickness, doApply);
	}



	public static void DrawLineH(this Texture2D tex, float xStart, float xEnd, float y, Color col, int thickness = 1, bool doApply = false)
	{
		int xStart2 = (int)Mathf.Clamp(Mathf.Min(xStart, xEnd), 0, tex.width - 1);
		int xEnd2 = (int)Mathf.Clamp(Mathf.Max(xStart, xEnd), 0, tex.width - 1);

		for (int yOffset = 0; yOffset < thickness; ++yOffset)
		{
			for (int x = xStart2; x <= xEnd2; ++x)
			{
				if (y + yOffset >= tex.height - 1)
					continue;

				tex.SetPixel(x, (int)y + yOffset, col);
			}
		}

		if (doApply) {
			tex.Apply();
		}
	}



	public static void DrawLineV(this Texture2D tex, float x, Color col, int thickness = 1, bool doApply = false)
	{
		DrawLineV(tex, 0, tex.height, x, col, thickness, doApply);
	}



	public static void DrawLineV(this Texture2D tex, float yStart, float yEnd, float x, Color col, int thickness = 1, bool doApply = false)
	{
		if (x < 0 || x >= tex.width)
			return;

		int yStart2 = (int)Mathf.Clamp(Mathf.Min(yStart, yEnd), 0, tex.height - 1);
		int yEnd2 = (int)Mathf.Clamp(Mathf.Max(yStart, yEnd), 0, tex.height - 1);

		for (int xOffset = -(thickness - 1) / 2; xOffset <= (thickness - 1) / 2; ++xOffset)
		{
			for (int y = yStart2; y <= yEnd2; ++y)
			{
				if (x + xOffset >= tex.width - 1)
					continue;

				tex.SetPixel((int)(x + xOffset), y, col);
			}
		}

		if (doApply) {
			tex.Apply();
		}
	}



	public static Texture2D DrawWave(
		this Texture2D tex,
		AudioClip clip,
		Color? backgroundColor = null,
		Color? borderColor = null,
		Color? lineColor = null,
		Color? lineColorEdge = null,
		int borderThicknessH = 1,
		int borderThicknessV = 1,
		bool onlyLeftChannel = false,
		double lengthBars = 0,
		double timeMarkersIntervalBars = 0,
		bool clearTex = true,
		float gain = 1,
		float timeStart = 0,
		float timeEnd = float.MaxValue,
		bool isRange01 = false
	)
	{
		if (!clip) {
			Debug.LogError("GetWaveTex: Supplied AudioClip was null");
			return null;
		}
		
		timeEnd = Mathf.Min(timeEnd, clip.length);
		float[] samples = new float[(int)(timeEnd - timeStart) * clip.frequency * clip.channels];
		clip.GetData(samples, (int)(timeStart * clip.frequency));


		return DrawWave(
			tex,
			samples,
			clip.channels,
			onlyLeftChannel,
			backgroundColor,
			borderColor,
			lineColor,
			lineColorEdge,
			borderThicknessH,
			borderThicknessV,
			lengthBars,
			timeMarkersIntervalBars,
			clearTex,
			gain,
			isRange01
		);
	}



	public static Texture2D DrawWave(
		this Texture2D tex,
		float[] samples,
		int channels,
		bool leftChannelOnly = false,
		Color? backgroundColor = null,
		Color? borderColor = null,
		Color? lineColor = null,
		Color? lineColorEdge = null,
		int borderThicknessH = 1,
		int borderThicknessV = 1,
		double lengthBars = 0,
		double timeMarkersIntervalBars = 0,
		bool clearTex = false,
		float gain = 1,
		bool isRange01 = false
	)
	{
		if (!lineColor.HasValue)
			lineColor = Color.white;

		if (!lineColorEdge.HasValue)
			lineColorEdge = lineColor;

		if (clearTex)
			GetRectTex(tex, backgroundColor ?? Color.clear, borderColor ?? Color.clear, borderThicknessH, borderThicknessV, false);

		Rect innerRect = new Rect(borderThicknessV, borderThicknessH, tex.width - borderThicknessV * 2, tex.height - borderThicknessH * 2);
		
		int numSamples = Mathf.Max(samples.Length / channels, 0);
		
		int channelsToDraw = leftChannelOnly ? 1 : channels;
		
		int prevDataIndex = 0;
		float yOffsetMax = innerRect.height * ((isRange01 ? 1 : 0.5f) / channelsToDraw);
		
		for (int channel = 0; channel < channelsToDraw; ++channel)
		{
			// get centre
			float yCentre = innerRect.yMax - (((isRange01 ? 1 : 0.5f) + channel) * innerRect.height / channelsToDraw);
			int yMax = Mathf.FloorToInt(yCentre + yOffsetMax - 1);
			int yMin = Mathf.CeilToInt(yCentre - yOffsetMax);
			
			for (int pixelX = borderThicknessH; pixelX < tex.width - borderThicknessH; ++pixelX)
			{
				// get sample index
				float xNorm = Mathf.InverseLerp(borderThicknessH, tex.width - borderThicknessH, pixelX);
				int dataIndex = (int)(xNorm * (numSamples - 1)) * channels + channel;

				// get sample
				float sampleMin = float.MaxValue;
				float sampleMax = float.MinValue;
				for (int i = prevDataIndex; i <= dataIndex; i += channels) {
					sampleMin = Mathf.Min(sampleMin, samples[i] * gain);
					sampleMax = Mathf.Max(sampleMax, samples[i] * gain);
				}

				if (isRange01)
					sampleMin = Mathf.Max(0, sampleMin);

				prevDataIndex = dataIndex;

				// sample to y
				int yLow = Mathf.RoundToInt(Mathf.Lerp(yMin, yMax, Mathf.InverseLerp(-1, 1, sampleMin)));
				int yHigh = Mathf.RoundToInt(Mathf.Lerp(yMin, yMax, Mathf.InverseLerp(-1, 1, sampleMax)));

				// draw line
				for (int y = yLow; y <= yHigh; ++y)
				{
					Color col =
						MathUtils.IsInClosedRange(xNorm * numSamples, 0, samples.Length) ?
						Color.Lerp(lineColor.Value, lineColorEdge.Value, Mathf.Abs((y - yCentre) / yOffsetMax)) :
						Color.gray;

					tex.SetPixel(pixelX, y, col);
				}
			}
		}

		
		// time markers
		if (lengthBars > 0 && timeMarkersIntervalBars > 0)
		{
			double currTime;

			for (int currMarker = 1; (currTime = currMarker * timeMarkersIntervalBars) < lengthBars; ++currMarker)
			{
				DrawLineV(
					tex,
					borderThicknessV,
					tex.height - borderThicknessV - 1,
					(int)Mathf.Lerp(borderThicknessH, tex.width - borderThicknessH - 1, (float)(currTime / lengthBars)),
					currTime % 1 == 0 ? Color.white : Color.gray,
					currTime % 1 == 0 ? 2 : 1,
					false
				);
			}
		}

		return tex;
	}



	public static void DrawLine(this Texture2D tex, float[] line, Color? col = null)
	{
		if (col == null) col = Color.white;

		for (int i = 0; i < line.Length - 1; ++i)
		{
			tex.DrawLine(
				xStart:	tex.width * ((i + 0) / (float)line.Length),
				xEnd:	tex.width * ((i + 1) / (float)line.Length),
				yStart:	tex.height * line[i + 0],
				yEnd:	tex.height * line[i + 1],
				col:	col.Value
			);
		}
	}



	public static Texture2D Fill(this Texture2D tex, Color col, bool doApply = false)
	{
		Color[] colors = new Color[tex.width * tex.height];

		for (int i = colors.Length - 1; i >= 0; --i)
			colors[i] = col;

		tex.SetPixels(colors);

		if (doApply)
			tex.Apply();

		return tex;
	}



	public static void DrawColorArray(this Texture2D tex, Color[] cols)
	{
		float invWidth = 1f / (tex.width - 1);

		for (int x = 0; x < tex.width; ++x) {
			Color col = cols[(int)((cols.Length - 1) * (x * invWidth))];
			for (int y = 0; y < tex.height; ++y)
				tex.SetPixel(x, y, col);
		}

		tex.Apply();
	}


	public static void DrawRect(Rect r, Color col)
	{
		GUI.DrawTexture(
			r,
			Texture2D.whiteTexture,
			ScaleMode.StretchToFill,
			true,
			1,
			col,
			0,
			0
		);
	}

}
