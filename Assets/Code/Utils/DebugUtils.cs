﻿using UnityEngine;

public static class DebugUtils
{

	public static void Log(Color col, string message, params object[] args)
	{
		Debug.Log(GetLogString(col, message, args));
	}


	public static void LogWarning(Color col, string message, params object[] args)
	{
		Debug.LogWarning(GetLogString(col, message, args));
	}
	

	public static void LogError(Color col, string message, params object[] args)
	{
		Debug.LogError(GetLogString(col, message, args));
	}


	private static string GetLogString(Color col, string message, params object[] args)
	{
		return string.Format(
			"<color=#{0}>{1}</color>",
			ColorUtility.ToHtmlStringRGB(col),
			string.Format(message, args)
		);
	}
}
