﻿using System;
using System.Collections.Generic;
using UnityEngine;


public static class AudioUtils
{

	private static readonly LowPassFilter lpFilter = new LowPassFilter();
	private static readonly PeakFilter peakFilter = new PeakFilter();

	
	/// <summary>Returns the number of samples set in 'wave'</summary>
	public static int GetWaveArray(this AudioClip clip, ref float[] wave, int sampleRate, float timeStart = 0, float timeEnd = float.MaxValue, float peakDecayAcceleration = 0, float lpCutoffFreq = 0, int lpOrder = 1)
	{
		timeEnd = Mathf.Min(timeEnd, clip.length);
		
		// LP filter
		bool doApplyLP = lpCutoffFreq > 0 && lpOrder > 0;
		if (doApplyLP)
		{
			lpFilter.Reset();
			lpFilter.SampleRate = sampleRate;
			lpFilter.CutoffFreq = lpCutoffFreq;
			lpFilter.Order = lpOrder;
		}
		
		// Peak filter
		bool doApplyPeak = peakDecayAcceleration > 0;
		if (doApplyPeak)
		{
			peakFilter.Reset();
			peakFilter.SampleRate = sampleRate;
			peakFilter.DecayAcceleration = peakDecayAcceleration;
		}
		
		
		int bufferLengthSamples = (int)(bufferLengthSeconds_GetWaveArray * clip.frequency);
		ArrayUtils.EnsureArrayLength(ref buffer_GetWaveArray, bufferLengthSamples * clip.channels);

		int waveLength = (int)(timeEnd * sampleRate) - (int)(timeStart * sampleRate);
		ArrayUtils.EnsureArrayLengthAtLeast(ref wave, waveLength);
		int waveIndex = 0;
		wave.SetZero();

		foreach (float sample in clip.IterateSamples(sampleRate, buffer_GetWaveArray, timeStart, timeEnd))
		{
			float sampleFiltered = sample;

			if (doApplyLP)
				sampleFiltered = lpFilter.Update(sampleFiltered);

			if (doApplyPeak)
				sampleFiltered = peakFilter.Update(sampleFiltered);

			wave[waveIndex++] = sampleFiltered;
		}

		return waveLength;
	}
	private const float bufferLengthSeconds_GetWaveArray = 0.1f;
	private static float[] buffer_GetWaveArray = new float[(int)(bufferLengthSeconds_GetWaveArray * 44100 * 2)];



	public static int SamplesInBars(float bars, decimal bpm, float sampleRate)
	{
		return Mathf.RoundToInt(bars * 4 * 60 * sampleRate / (float)bpm);
	}


	public static float SecondsPerBar(decimal bpm)
	{
		return 4 * 60 / (float)bpm;
	}


	public static int GetNumBars(int samples, int sampleRate, decimal bpm)
	{
		return (int)((samples / (float)sampleRate) / SecondsPerBar(bpm));
	}


	public static decimal SpbToBpm(float secondsPerBar)
	{
		return (decimal)(4 * 60 / secondsPerBar);
	}


	public static decimal[] GetBpmRange(decimal min, decimal max, decimal interval)
	{
		decimal[] ret = new decimal[Mathf.RoundToInt((float)((max - min) / interval)) + 1];

		for (int i = ret.Length - 1; i >= 0; --i)
			ret[i] = min + i * interval;

		return ret;
	}



	public static float GetBpmExponent(float peakDist)
	{
		return GetBpmExponent(SpbToBpm(peakDist));
	}



	public static float GetBpmExponent(decimal bpm)
	{
		return Mathf.Log((float)bpm, 2) % 1;
	}



	public static void GetStartAndEndOfNoise(this AudioClip clip, out float start, out float end, float threshold = 0.5f)
	{
		start = end = -1;
		int channels = clip.channels;
		float frequencyInv = 1 / (float)clip.frequency;
		int bufferLengthSamples = (int)(bufferLengthSeconds_GetStartAndEndOfNoise * clip.frequency);
		ArrayUtils.EnsureArrayLength(ref buffer_GetStartAndEndOfNoise, bufferLengthSamples * channels);
		float[] buffer = buffer_GetStartAndEndOfNoise;


		// Start
		int sampleIndexAtStartOfBuffer = 0;
		bool startFound = false;

		while (!startFound)
		{
			clip.GetData(buffer, sampleIndexAtStartOfBuffer);

			for (int i = 0; i < bufferLengthSamples; ++i)
				if (buffer[i * channels] > threshold) {
					start = (sampleIndexAtStartOfBuffer + i) * frequencyInv;
					startFound = true;
					break;
				}

			sampleIndexAtStartOfBuffer += bufferLengthSamples;

			if (sampleIndexAtStartOfBuffer >= clip.samples)
				throw new Exception(string.Format("Non samples above threshold {0} found", threshold));
		}


		// End
		sampleIndexAtStartOfBuffer = clip.samples - bufferLengthSamples;
		bool endFound = false;

		while (!endFound)
		{
			clip.GetData(buffer, sampleIndexAtStartOfBuffer);

			for (int i = bufferLengthSamples - channels; i >= 0; --i)
				if (buffer[i * channels] > threshold) {
					end = (sampleIndexAtStartOfBuffer + i) * frequencyInv;
					endFound = true;
					break;
				}

			sampleIndexAtStartOfBuffer -= buffer.Length;

			if (sampleIndexAtStartOfBuffer < 0)
				throw new Exception(string.Format("Non samples above threshold {0} found", threshold));
		}
	}
	private const float bufferLengthSeconds_GetStartAndEndOfNoise = 0.1f;
	private static float[] buffer_GetStartAndEndOfNoise = new float[(int)(bufferLengthSeconds_GetStartAndEndOfNoise * 44100 * 2)];



	public static bool TryGetBpmFromClipName(this AudioClip clip, ref decimal bpm)
	{
		if (clip.name.Length < 7)
			return false;

		decimal bpmTmp = 0;
		if (decimal.TryParse(clip.name.Substring(0, 7), out bpmTmp)) {
			bpm = bpmTmp;
			return true;
		}

		return false;
	}


	
	public static IEnumerable<float> IterateSamples(this AudioClip clip, int sampleRate, float[] buffer, float timeStart = 0, float timeEnd = float.MaxValue)
	{
		timeEnd = Mathf.Min(timeEnd, clip.length);
		int resampleIndexMin = (int)(timeStart * sampleRate);
		int resampledIndexMaxExclusive = (int)(timeEnd * sampleRate);
		int channels = clip.channels;
		int bufferLengthSamples = buffer.Length / channels;
		float resampledIndexToClipSampleIndex = (float)clip.frequency / sampleRate;
		int maxClipSampleInBuffer = (int)(resampleIndexMin * resampledIndexToClipSampleIndex);
		int minClipSampleInBuffer = maxClipSampleInBuffer - bufferLengthSamples;

		for (int resampleIndex = resampleIndexMin; resampleIndex < resampledIndexMaxExclusive; ++resampleIndex)
		{
			int clipSampleIndex = (int)(resampleIndex * resampledIndexToClipSampleIndex);
			
			if (clipSampleIndex >= maxClipSampleInBuffer)
			{
				// Clamp the buffer length so that it doesn't wrap around and start taking samples from the start of clip
				int samplesRemaining = clip.samples - maxClipSampleInBuffer;
				if (samplesRemaining < bufferLengthSamples)
				{
					buffer = new float[samplesRemaining * channels];
				}

				clip.GetData(buffer, maxClipSampleInBuffer);
				maxClipSampleInBuffer += bufferLengthSamples;
				minClipSampleInBuffer += bufferLengthSamples;
			}

			yield return buffer[(clipSampleIndex - minClipSampleInBuffer) * channels];
		}
	}



	public static float GetCutoffAlphaLowPass(int sampleRate, float cutoffFreq)
	{
		float dt = 1f / sampleRate;
		float RC = 1f / (MathUtils.TAU * cutoffFreq);
		return dt / (RC + dt);
	}



	public static float GetCutoffAlphaHighPass(int sampleRate, float cutoffFreq)
	{
		return 1 - GetCutoffAlphaLowPass(sampleRate, cutoffFreq);
	}



	public static float[] GetAverageBar(this float[] samples, int sampleRate, decimal bpm)
	{
		int samplesPerBar = SamplesInBars(1, bpm, sampleRate);
		int numBars = samples.Length / samplesPerBar;
		float numBarsInv = 1f / numBars;

		float[] ret = new float[samplesPerBar];
		for (int i = numBars * samplesPerBar - 1; i >= 0; --i)
			ret[i % samplesPerBar] += samples[i] * numBarsInv;

		return ret;
	}


	public static void LimitMagnitude(this float[] samples, float value = 1)
	{
		for (int i = samples.Length - 1; i >= 0; --i) {
			if (samples[i] > value) samples[i] = value;
			if (samples[i] < -value) samples[i] = -value;
		}
	}



	public static float GetNumBars(this AudioClip targ, decimal bpm)
	{
		return GetNumBars(targ.samples, targ.frequency, bpm);
	}



	public static float[] ApplyFilter(this float[] targ, AudioFilter filter)
	{
		filter.Apply(targ);
		return targ;
	}



	public static float[] RemoveDCOffset(this float[] targ)
	{
		return targ.Add(-targ.GetAverage());
	}


}
