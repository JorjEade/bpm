﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GUIUtils
{

	public static void DrawRect(Rect r, Color? col = null)
	{
		GUI.DrawTexture(
			r,
			Texture2D.whiteTexture,
			ScaleMode.StretchToFill,
			true,
			1,
			col ?? Color.white,
			0,
			0
		);
	}

}
