﻿using System;
using System.Collections.Generic;
using UnityEngine;


public static class ArrayUtils
{

	public static int GetIndexOfMin(this IList<float> targ, int indexStart = 0, int indexEnd = int.MaxValue, bool wrap = false)
	{
		if (wrap)
		{
			if (indexEnd == int.MaxValue)
				indexEnd = targ.Count - 1;
		}
		else
		{
			indexStart = Mathf.Max(indexStart, 0);
			indexEnd = Mathf.Min(indexEnd, targ.Count - 1);
		}

		float minValue = float.MaxValue;
		int indexOfMin = -1;

		for (int i = indexStart; i <= indexEnd; ++i)
		{
			int indexWrapped = MathUtils.Wrap(i, targ.Count);

			if (targ[indexWrapped] < minValue) {
				minValue = targ[indexWrapped];
				indexOfMin = indexWrapped;
			}
		}

		return indexOfMin;
	}


	public static int GetIndexOfMax(this IList<float> targ, int indexStart = 0, int indexEnd = int.MaxValue, bool wrap = false)
	{
		if (wrap)
		{
			if (indexEnd == int.MaxValue)
				indexEnd = targ.Count - 1;
		}
		else
		{
			indexStart = Mathf.Max(indexStart, 0);
			indexEnd = Mathf.Min(indexEnd, targ.Count - 1);
		}

		float maxValue = float.MinValue;
		int indexOfMax = -1;

		for (int i = indexStart; i <= indexEnd; ++i)
		{
			int indexWrapped = MathUtils.Wrap(i, targ.Count);

			if (targ[indexWrapped] > maxValue) {
				maxValue = targ[indexWrapped];
				indexOfMax = indexWrapped;
			}
		}

		return indexOfMax;
	}


	public static int GetIndexOfMax(int indexStart, int indexEnd, bool wrap, params IList<float>[] arrays)
	{
		int indexOfMax = -1;
		float maxVal = float.MinValue;

		foreach (var array in arrays)
		{
			int tmpIdxOfMax = array.GetIndexOfMax(indexStart, indexEnd, wrap);
			float tmpMaxValue = array[tmpIdxOfMax];

			if (tmpMaxValue > maxVal) {
				maxVal = tmpMaxValue;
				indexOfMax = tmpIdxOfMax;
			}
		}

		return indexOfMax;
	}


	public static float GetMin(this float[] targ, int startIndex = 0, int endIndex = int.MaxValue, bool wrap = false)
	{
		return targ[targ.GetIndexOfMin(startIndex, endIndex, wrap)];
	}


	public static float GetMax(this float[] targ, int startIndex = 0, int endIndex = int.MaxValue, bool wrap = false)
	{
		return targ[targ.GetIndexOfMax(startIndex, endIndex, wrap)];
	}


	public static T Second<T>(this IList<T> targ)
	{
		return targ[1];
	}


	public static T SecondToLast<T>(this IList<T> targ)
	{
		return targ[targ.Count - 2];
	}


	public static T ThirdToLast<T>(this IList<T> targ)
	{
		return targ[targ.Count - 3];
	}


	public static float Range(this float[] targ)
	{
		return targ[targ.Length - 1] - targ[0];
	}


	public static float[] Smooth(this float[] targ, int valuesEitherSide, int passes = 1)
	{
		int newStartIndex = 0;
		int newLength = targ.Length;
		targ.Smooth_Internal(valuesEitherSide, ref newStartIndex, ref newLength, passes);

		// Clamp start
		for (int i = 0; i < newStartIndex; ++i)
			targ[i] = targ[newStartIndex];

		// Clamp end
		for (int i = targ.Length - 1; i > newLength - 1; --i)
			targ[i] = targ[newLength - 1];

		return targ;
	}


	private static void Smooth_Internal(this float[] targ, int valuesEitherSide, ref int newStartIndex, ref int newLength, int passes)
	{
		if (passes == 0)
			return;

		EnsureArrayLengthAtLeast(ref dummyFloatArray_Smooth_Internal, targ.Length);

		targ.CopyTo(dummyFloatArray_Smooth_Internal, 0);

		newStartIndex += valuesEitherSide;
		newLength -= valuesEitherSide;
		
		int range = valuesEitherSide * 2 + 1;

		for (int i = newLength - 1; i >= newStartIndex; --i)
		{
			float smoothValue = 0;
			for (int offset = -valuesEitherSide; offset <= valuesEitherSide; ++offset)
				smoothValue += dummyFloatArray_Smooth_Internal[i + offset];
			targ[i] = smoothValue / range;
		}

		targ.Smooth_Internal(valuesEitherSide, ref newStartIndex, ref newLength, passes - 1);
	}
	private static float[] dummyFloatArray_Smooth_Internal = new float[30000];



	public static float[] SmoothCyclic(this float[] targ, int valuesEitherSide, int passes = 1)
	{
		EnsureArrayLengthAtLeast(ref dummyFloatArray_Smooth_Internal, targ.Length);

		targ.CopyTo(dummyFloatArray_Smooth_Internal, 0);

		float rangeInv = 1f / (valuesEitherSide * 2 + 1);

		for (int i = targ.Length - 1; i >= 0; --i)
		{
			float smoothValue = 0;
			for (int offset = -valuesEitherSide; offset <= valuesEitherSide; ++offset)
				smoothValue += dummyFloatArray_Smooth_Internal[(i + offset + targ.Length) % targ.Length];
			targ[i] = smoothValue * rangeInv;
		}

		if (passes > 1)
			targ.SmoothCyclic(valuesEitherSide, passes - 1);

		return targ;
	}



	public static T[] GetTrimmed<T>(this T[] targ, int leftRight)
	{
		return targ.GetTrimmed(leftRight, leftRight);
	}


	public static T[] GetTrimmed<T>(this T[] targ, int left, int right)
	{
		T[] ret = new T[targ.Length - left - right];
		for (int i = left; i < targ.Length - right; ++i)
			ret[i - left] = targ[i];
		return ret;
	}


	public static float[] NormaliseRange(this float[] targ)
	{
		float min = float.MaxValue;
		float max = float.MinValue;
		for (int i = targ.Length - 1; i >= 0; --i) {
			if (targ[i] < min) min = targ[i];
			if (targ[i] > max) max = targ[i];
		}

		if (min == max)
			return targ;
		
		for (int i = targ.Length - 1; i >= 0; --i)
			targ[i] = (targ[i] - min) / (max - min);

		return targ;
	}


	public static void Invert01(this float[] targ)
	{
		for (int i = targ.Length - 1; i >= 0; --i)
			targ[i] = 1 - targ[i];
	}


	public static float[] NormaliseMagnitude(this float[] targ)
	{
		float magMaxSq = 0;
		for (int i = targ.Length - 1; i >= 0; --i)
			if (targ[i] * targ[i] > magMaxSq)
				magMaxSq = targ[i] * targ[i];

		if (magMaxSq == 0)
			return targ;
		
		float magMaxInv = 1f / Mathf.Sqrt(magMaxSq);

		for (int i = targ.Length - 1; i >= 0; --i)
			targ[i] *= magMaxInv;

		return targ;
	}


	public static float[] Abs(this float[] targ)
	{
		for (int i = targ.Length - 1; i >= 0; --i)
			if (targ[i] < 0)
				targ[i] = -targ[i];

		return targ;
	}


	public static float[] ClampAbove(this float[] targ, float value)
	{
		for (int i = targ.Length - 1; i >= 0; --i)
			if (targ[i] < value)
				targ[i] = value;

		return targ;
	}


	public static float[] ClampBelow(this float[] targ, float value)
	{
		for (int i = targ.Length - 1; i >= 0; --i)
			if (targ[i] > value)
				targ[i] = value;

		return targ;
	}


	public static void OffsetValues(this decimal[] targ, decimal offset)
	{
		for (int i = targ.Length - 1; i >= 0; --i)
			targ[i] += offset;
	}


	public static int FindPeakOrTrough(this float[] targ, int index, bool isPeak)
	{
		int peakTroughInt = isPeak ? 1 : -1;
		int indexL = index == targ.Length - 1 ? targ.Length - 2 : index;
		int indexR = indexL + 1;
		int gradient = MathUtils.SignOrZero(targ[indexR] - targ[indexL]) * peakTroughInt;

		while (
			(index + gradient) >= 0 &&
			(index + gradient) <= targ.Length - 1 &&
			MathUtils.SignOrZero(targ[index + gradient] - targ[index]) == peakTroughInt
		) {
			index += gradient;
		}

		return index;
	}


	public static int FindPeakOrTroughWrap(this float[] targ, int index, bool isPeak)
	{
		int peakTroughInt = isPeak ? 1 : -1;

		int startingGradient = MathUtils.SignOrZero(
			targ[MathUtils.Wrap(index + 1, targ.Length)] -
			targ[MathUtils.Wrap(index, targ.Length)]
		);

		if (startingGradient == 0)
		{
			startingGradient = MathUtils.SignOrZero(
				targ[MathUtils.Wrap(index, targ.Length)] -
				targ[MathUtils.Wrap(index - 1, targ.Length)]
			);
		}

		int direction = startingGradient * peakTroughInt;

		while (
			MathUtils.SignOrZero(
				targ[MathUtils.Wrap(index + 1, targ.Length)] -
				targ[MathUtils.Wrap(index, targ.Length)]
			) == startingGradient
		) {
			index += direction;
		}

		return MathUtils.Wrap(index, targ.Length);
	}


	public static int FindPeak(this float[] targ, int index, bool wrap = false)
	{
		return wrap
			? targ.FindPeakOrTroughWrap(index, true)
			: targ.FindPeakOrTrough(index, true);
	}


	public static int FindTrough(this float[] targ, int index, bool wrap = false)
	{
		return wrap
			? targ.FindPeakOrTroughWrap(index, false)
			: targ.FindPeakOrTrough(index, false);
	}


	public static bool EnsureArrayLength<T>(ref T[] array, int targetLength)
	{
		if (array.Length == targetLength)
			return true;

		Debug.LogWarningFormat("Array length changed! {0} to {1}", array.Length, targetLength);
		array = new T[targetLength];

		return false;
	}


	public static bool EnsureArrayLengthAtLeast<T>(ref T[] array, int targetLength)
	{
		if (array.Length >= targetLength)
			return true;

		Debug.LogWarningFormat("Array length changed! {0} to {1}", array.Length, targetLength);
		array = new T[targetLength];

		return false;
	}


	public static void SetZero(this float[] targ)
	{
		for (int i = targ.Length - 1; i >= 0; --i)
			targ[i] = 0;
	}



	public static void SetZero(this double[] targ)
	{
		for (int i = targ.Length - 1; i >= 0; --i)
			targ[i] = 0;
	}



	public static float GetSDSqr(this float[] targ, int startIndex = 0, int endIndex = int.MaxValue)
	{
		if (endIndex == int.MaxValue)
			endIndex = targ.Length - 1;

		// Get average
		float avg = 0;
		for (int i = startIndex; i <= endIndex; ++i) {
			float targVal = i < 0 || i > targ.Length - 1 ? 0 : targ[i];
			avg += targVal;
		}
		avg /= (endIndex - startIndex + 1);

		// Get sum of sqr diffs
		float sumOfSqrDiffs = 0;
		for (int i = startIndex; i <= endIndex; ++i) {
			float targVal = i < 0 || i > targ.Length - 1 ? 0 : targ[i];
			sumOfSqrDiffs += MathUtils.Square(targVal - avg);
		}
		
		return sumOfSqrDiffs / (endIndex - startIndex + 1);
	}


	public static float GetSD(this float[] targ, int startIndex = 0, int endIndex = int.MaxValue)
	{
		return Mathf.Sqrt(targ.GetSDSqr(startIndex, endIndex));
	}


	public static float GetGcdOfIntervals(this IList<float> targ)
	{
		float gcd = targ[1] - targ[0];

		int numGcds = 1;

		for (int i = 2; i < targ.Count; ++i)
		{
			float distFromPrev = targ[i] - targ[i - 1];
			float numGcdsInDistFromPrev = Mathf.Max(distFromPrev, gcd) / Mathf.Min(distFromPrev, gcd);

			if (MathUtils.DistFromNearestInteger(numGcdsInDistFromPrev) > 0.1f)
			{
				//Debug.LogError("! " + DistFromNearestInteger(numGcdsInDistFromPrev));
				return 0;
			}

			if (distFromPrev <  gcd)
			{
				numGcds *= Mathf.RoundToInt(numGcdsInDistFromPrev);
				++numGcds;
			}
			else
			{
				numGcds += Mathf.RoundToInt(numGcdsInDistFromPrev);
			}

			gcd = (targ[i] - targ[0]) / numGcds;
		}

		return gcd;
	}


	public static float GetAverageOfSuccessiveSqDiffs(this float[] targ, int valuesLength, int frameLength, int indexIncrement = 1, int startIndex = 0)
	{
		int numFrames = (valuesLength - startIndex - frameLength) / indexIncrement;
		int indexMax = startIndex + (numFrames - 1) * indexIncrement;
		float sumOfSqDiffs = 0;

		for (int index = startIndex; index <= indexMax; index += indexIncrement)
		{
			float diff = targ[index] - targ[index + frameLength];
			sumOfSqDiffs += diff * diff;
		}

		return sumOfSqDiffs / numFrames;
	}


	public static float GetAverage(this float[] targ, int startIndex = 0, int endIndex = int.MaxValue)
	{
		if (endIndex == int.MaxValue)
			endIndex = targ.Length - 1;

		float total = 0;

		for (int i = startIndex; i <= endIndex; ++i) {
			float targVal = i < 0 || i > targ.Length - 1 ? 0 : targ[i];
			total += targVal;
		}
		
		return total / (endIndex - startIndex + 1);
	}


	public static float[] Multiply(params float[][] targs)
	{
		float[] ret = new float[targs[0].Length];

		for (int i = 0; i < ret.Length; ++i)
		{
			ret[i] = 1;

			for (int targIndex = 0; targIndex < targs.Length; ++targIndex)
			{
				ret[i] *= targs[targIndex][i];
			}
		}

		return ret;
	}


	public static float[] Multiply(this float[] targ, float v)
	{
		for (int i = targ.Length - 1; i >= 0; --i)
			targ[i] *= v;

		return targ;
	}


	public static float[] Add(params float[][] targs)
	{
		float[] ret = new float[targs[0].Length];

		for (int i = 0; i < ret.Length; ++i)
			for (int targIndex = 0; targIndex < targs.Length; ++targIndex)
				ret[i] += targs[targIndex][i];

		return ret;
	}


	public static float[] Add(this float[] targ, float val)
	{
		for (int i = targ.Length - 1; i >= 0; --i)
			targ[i] += val;

		return targ;
	}


	public static Color[] GetColArray(float[] r, float[] g, float[] b)
	{
		Color[] ret = new Color[r.Length];

		for (int i = ret.Length - 1; i >= 0; --i)
			ret[i] = new Color(r[i], g[i], b[i]);

		return ret;
	}


	public static float[] GetShifted(this float[] targ, int offset)
	{
		float[] ret = new float[targ.Length];
		for (int i = 0; i < ret.Length; ++i)
			ret[MathUtils.Wrap(i + offset, ret.Length)] = targ[i];
		return ret;
	}



	public static bool IsPeak(this float[] targ, int index, int halfWidth = 1, float height = 0)
	{
		return
			targ[index] >= targ[MathUtils.Wrap(index - halfWidth, targ.Length)] + height &&
			targ[index] >= targ[MathUtils.Wrap(index + halfWidth, targ.Length)] + height;
	}



	public static float[] GetSuccessiveLRDiffs(this float[] targ, IList<int> offsets, IList<float> offsetsWeights, int halfSpread, int incr = 1)
	{
		float[] ret = new float[offsets.Count];

		for (int iOffsets = 0; iOffsets < offsets.Count; ++iOffsets)
		{
			for (int iTarg = offsets[iOffsets]; iTarg < targ.Length; iTarg += incr)
			{
				float diff =
					targ.GetAverage(iTarg, iTarg + halfSpread) -
					targ.GetAverage(iTarg - halfSpread, iTarg);

				if (diff > 0)
					ret[iOffsets] += MathUtils.Pow5(diff) * offsetsWeights[iOffsets];
			}
		}

		return ret;
	}



	public static float GetCertaintyOfMax(this float[] targ)
	{
		if (targ.Length == 0) {
			Debug.LogWarning("Array length is zero - returning 1");
			return 1;
		}

		if (targ.Length == 1)
			return 1;

		float max = float.MinValue;
		float max2 = float.MinValue;

		foreach (var v in targ)
		{
			if (v > max) {
				max2 = max;
				max = v;
			}
			else if (v > max2)
				max2 = v;
		}

		return max / (max + max2);
	}



	public static float[] GetCopy(this float[] targ)
	{
		float[] ret = new float[targ.Length];
		targ.CopyTo(ret, 0);
		return ret;
	}


	public static float[] GetLRDiffs(this float[] targ, int range)
	{
		float[] ret = new float[targ.Length];
		float invRangePlus1 = 1f / (range + 1);

		float totalL = 0;
		float totalR = 0;

		for (int i = -range; i < targ.Length; ++i)
		{
			// Update R total
			if (i + range < targ.Length)
				totalR += targ[i + range];
			if (i >= 1)
				totalR -= targ[i - 1];

			// Update L total
			if (i >= 0)
				totalL += targ[i];
			if (i - range >= 1)
				totalL -= targ[i - range - 1];

			// Add diff to ret
			if (i >= 0)
				ret[i] = (totalR - totalL) * invRangePlus1;
		}

		return ret;
	}



	public static float[] GetGradients(this float[] targ)
	{
		float[] ret = new float[targ.Length];

		for (int i = 1; i < targ.Length; ++i)
			ret[i] = targ[i] - targ[i - 1];

		return ret;
	}

}
