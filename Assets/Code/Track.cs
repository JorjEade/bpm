﻿#pragma warning disable 0162

using System;
using System.Collections;
using System.IO;
using UnityEngine;


[Serializable]
public class Track
{
	private const bool ENABLE_LOG = false;
	private static readonly Color logCol = new Color(0, 0, 0.5f);

	public int index;
	public string name;
	public string path;
	public decimal bpm = Detector.INVALID_VALUE;
	public int downbeatSample = (int)Detector.INVALID_VALUE;
	public decimal downbeatTime = Detector.INVALID_VALUE;

	public AudioDataLoadState LoadingState { get; private set; }

	private AudioClip clip;
	public AudioClip Clip
	{
		get { return clip; }

		set
		{
			clip = value;

			if (clip == null)
			{
				LoadingState = AudioDataLoadState.Unloaded;
				return;
			}

			LoadingState = clip.loadState;

			// Try set BPM from clip name
			if (bpm == Detector.INVALID_VALUE)
				if (clip.TryGetBpmFromClipName(ref bpm))
					DebugUtils.LogWarning(logCol, "Setting BPM from name: " + bpm);

			
			// Set name
			if (string.IsNullOrEmpty(name)) {
				Debug.LogWarning("Setting name from clip name: " + clip.name);
				name =  clip.name;
			}
		}
	}


	public IEnumerator LoadClip()
	{
		// First, check if clip is set but its audio data not loaded
		if (clip != null && clip.loadState == AudioDataLoadState.Unloaded)
		{
			if (ENABLE_LOG) DebugUtils.Log(logCol, "Loading audio data for clip \"{0}\"...", clip.name);

			if (clip.LoadAudioData())
			{
				if (ENABLE_LOG) DebugUtils.Log(logCol, "...Loaded");
			}
			else
			{
				DebugUtils.LogError(logCol, "...Error loading audio data from clip: {0}", clip.name);
			}

			LoadingState = clip.loadState;
			Clip = clip;
			yield break;
		}


		// Otherwise, load from path
		switch (LoadingState)
		{
			case AudioDataLoadState.Loading:
				DebugUtils.LogWarning(logCol, "Track already loading");
				yield break;

			case AudioDataLoadState.Loaded:
				DebugUtils.LogWarning(logCol, "Track already loaded");
				yield break;
		}

		LoadingState = AudioDataLoadState.Loading;

		if (ENABLE_LOG) DebugUtils.Log(logCol, "Loading \"{0}\"...", path);

		using (WWW www = new WWW(path))
		{
			yield return www;

			if (!string.IsNullOrEmpty(www.error)) {
				DebugUtils.LogError(logCol, "...Error loading {0}: {1}", path, www.error);
				LoadingState = AudioDataLoadState.Failed;
				yield break;
			}

			LoadingState = AudioDataLoadState.Loaded;
			if (ENABLE_LOG) DebugUtils.Log(logCol, "...Loaded");

			AudioClip clipTmp = www.GetAudioClip();
			clipTmp.name = Path.GetFileNameWithoutExtension(path);
			Clip = clipTmp;
		}
	}


	public void UnloadClip()
	{
		if (Clip != null)
		{
			Clip.UnloadAudioData();
			GameObject.Destroy(Clip);
		}

		LoadingState = AudioDataLoadState.Unloaded;
	}

}