﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Detector
{

	public const decimal INVALID_VALUE = -1;
	private const float certaintyWarn = 0.5f;

	public bool Complete { get; private set; }
	public Track Track { get; private set; }
	public decimal DetectedValue { get; protected set; }
	public float Certainty { get; protected set; }
	public abstract decimal ActualValue { get; }
	public readonly List<Texture2D> debugTextures = new List<Texture2D>();

	static float totalCertainty = 0;
	static int numCertainty = 0;

	public IEnumerator Detect(Track track)
	{
		Track = track;

		DetectedValue = INVALID_VALUE;
		
		// Load track's clip if haven't already
		if (track.LoadingState == AudioDataLoadState.Unloaded)
			yield return track.LoadClip();

		while (track.LoadingState == AudioDataLoadState.Loading)
			yield return null;

		if (track.LoadingState == AudioDataLoadState.Failed)
			yield break;

		DateTime t = DateTime.Now;
		Debug.LogFormat("  <b>Detect:</b> {0} <b>Actual:</b> {1}", track.name, ActualValue);

		Complete = false;
		yield return DetectAbstract();
		Complete = true;

		string message = string.Format(
			"<b>/Detect: <color=#{1}>{0:0.000}</color>  Actual:</b> {2:0.000}  <b>Difference:</b> {3}	<b>Certainty:</b> <color=#{5}>{4:0.0%}</color>	<b>Time:</b> {6:0.000}  <b>{7}</b>",
			DetectedValue,
			ColorUtility.ToHtmlStringRGB(DetectedColor),
			ActualValue,
			ActualValue != INVALID_VALUE ? GetDifferenceFromActualValue(DetectedValue).ToString("+0.000;-0.000") : "N/A",
			Certainty,
			ColorUtility.ToHtmlStringRGB(Color.Lerp(Color.red, Color.black, Certainty)),
			(DateTime.Now - t).TotalSeconds,
			Track.name
		);

		if (ActualValue != INVALID_VALUE && !CloseEnough)
		{
			Debug.LogError(message);
		}
		else
		{
			if (Certainty < certaintyWarn)
				Debug.LogWarning(message);
			else
				Debug.Log(message);
			
			++numCertainty;
			totalCertainty += Certainty;
			Debug.LogFormat("Avg Certainty: {0}", totalCertainty / numCertainty);
		}
	}


	protected abstract IEnumerator DetectAbstract();

	public abstract bool CloseEnough { get; }


	public virtual decimal GetDifferenceFromActualValue(decimal v)
	{
		return v - ActualValue;
	}


	public Color DetectedColor
	{
		get
		{
			return
				!Complete || ActualValue == 0 || ActualValue == INVALID_VALUE ? Color.yellow
				: DetectedValue == ActualValue ? Color.green
				: CloseEnough ? new Color(0, 0.5f, 0)
				: Color.red;
		}
	}


	protected Texture2D AddDebugTex()
	{
		Texture2D tex = TexUtils.GetRectTex(Screen.width, Screen.height / 2, doApply: true);
		debugTextures.Add(tex);
		return tex;
	}

}
