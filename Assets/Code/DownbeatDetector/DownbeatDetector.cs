﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DownbeatDetector : Detector
{
	public float clipStartTime = 0;
	public float clipEndTime = float.MaxValue;
	public const int sampleRate = 2600;

	const int	filterOrder = 2;
	const float lpCutoff = 100;
	const float hpCutoff = 100;
	
//	const float avgBarSmoothingTimeLP = 0.02f;
	const float avgBarSmoothingTimeHP = 0.01f;
	const int	avgBarSmoothingPasses = 3;

	const float peakMinHalfWidthTime = 0.01f;
	const float peakMinHeight = 0.001f;
	const float peakLookBackTime = 0.03f;
	const float peakLookAheadTime = 0;
	const float peakAdjustmentTime = -0.01f;

	const float peakWeightPeakAccel = 1;
	const float asdBars = 0.5f;



	protected override IEnumerator DetectAbstract()
	{
		Func<float, int> Time2Samples = time => (int)(time * sampleRate);
		clipEndTime = Mathf.Min(clipEndTime, Track.Clip.length);
		int samplesPerBar = AudioUtils.SamplesInBars(1, Track.bpm, sampleRate);
		float secondsPerBar = AudioUtils.SecondsPerBar(Track.bpm);
		float[] peakWeights = null;
		int clipLengthSamples =			(int)(sampleRate * (clipEndTime - clipStartTime));
//		int avgBarSmoothingSamplesLP =	(int)(sampleRate * avgBarSmoothingTimeLP);
		int avgBarSmoothingSamplesHP =	(int)(sampleRate * avgBarSmoothingTimeHP);
		int peakMinHalfWidthSamples =	(int)(sampleRate * peakMinHalfWidthTime);
		int peakLookBackSamples =		(int)(sampleRate * peakLookBackTime);
		int peakLookAheadSamples =		(int)(sampleRate * peakLookAheadTime);
		int peakAdjustmentSamples =		(int)(sampleRate * peakAdjustmentTime);
		int asdSamples =				(int)(samplesPerBar * asdBars);
		

		// =============================== GET WAVES ===============================

		float[] waveOriginal = new float[clipLengthSamples];
		Track.Clip.GetWaveArray(ref waveOriginal, sampleRate, clipStartTime, clipEndTime);


		// ---------- LP ----------
		float[] waveLP = null;
		float[] avgBarLP = null;
		float[] avgBarLPSmoothed = null;

		Action calculateWavesLP = () =>
		{
			waveLP = waveOriginal.GetCopy()
				.ApplyFilter(new LowPassFilter() { SampleRate = sampleRate, Order = filterOrder, CutoffFreq = lpCutoff })
				.Abs()
				.NormaliseMagnitude();

			// Average bar
//			avgBarLP = waveLP.GetAverageBar(sampleRate, Track.bpm)
//				.NormaliseMagnitude();
//
//			// Average bar smoothed
//			avgBarLPSmoothed = avgBarLP.GetCopy()
//				.SmoothCyclic(avgBarSmoothingSamplesLP, avgBarSmoothingPasses)
//				.NormaliseRange();
		};

		// ---------- HP ----------
		float[] waveHP = null;
		float[] avgBarHP = null;
		float[] avgBarHPSmoothed = null;

		Action calculateWavesHP = () =>
		{
			waveHP = waveOriginal.GetCopy()
				.ApplyFilter(new HighPassFilter() { SampleRate = sampleRate, Order = filterOrder, CutoffFreq = hpCutoff })
				.Abs()
				.NormaliseMagnitude();

			// Average bar
			avgBarHP = waveHP.GetAverageBar(sampleRate, Track.bpm)
				.NormaliseMagnitude();

			// Average bar smoothed
			avgBarHPSmoothed = avgBarHP.GetCopy()
				.SmoothCyclic(avgBarSmoothingSamplesHP, avgBarSmoothingPasses)
				.NormaliseMagnitude();
		};

//		ThreadUtils.RunActionsThreaded(
//			calculateWavesLP,
//			calculateWavesHP
//		);
		calculateWavesLP();
		calculateWavesHP();


		// ========================== GET AVERAGE BAR PEAKS ==========================
		
		// Get average bar
		float[] avgBarForPeaks = avgBarHPSmoothed.GetCopy();
		avgBarForPeaks.NormaliseRange();
		

		// Get peaks
		List<int> avgBarPeakSmpIdxs = new List<int>();
		List<float> avgBarPeakValues = new List<float>();
		
		for (int i = 0; i < avgBarForPeaks.Length; ++i)
		{
			if (avgBarForPeaks.IsPeak(i) &&
				avgBarForPeaks.IsPeak(i, peakMinHalfWidthSamples, peakMinHeight))
			{
				int indexOfLocalMax = avgBarHP.GetIndexOfMax(i - peakLookBackSamples, i + peakLookAheadSamples, wrap: true);
				avgBarPeakSmpIdxs.Add(MathUtils.Wrap(indexOfLocalMax + peakAdjustmentSamples, samplesPerBar));
				avgBarPeakValues.Add(avgBarForPeaks[i]);
			}
		}

		for (int i = 0; i < avgBarForPeaks.Length; i += peakMinHalfWidthSamples) {
			//avgBarPeakSmpIdxs.Add(i);
			//avgBarPeakValues.Add(avgBar[i]);
		}

		// Add peak at ActualValue
		//avgBarPeakSmpIdxs.Add((int)(sampleRate * MathUtils.Wrap((float)ActualValue - clipStartTime, secondsPerBar)));
		//avgBarPeakValues.Add(0.5f);

		if (avgBarPeakSmpIdxs.Count == 0)
		{
			Debug.LogError("No peaks found!");
			goto debug_textures;
		}


		// ============================ GET PEAK WEIGHTS ============================

		float[] peakWeightsLP = null;
		float[] peakWeightsHP = null;

		ThreadUtils.RunActionsThreaded
		(
			() => {
				peakWeightsLP = waveLP.GetCopy()
					.ApplyFilter(new PeakFilter() { SampleRate = sampleRate, DecayAcceleration = peakWeightPeakAccel })
					.GetSuccessiveLRDiffs(avgBarPeakSmpIdxs, avgBarPeakValues, asdSamples, samplesPerBar);
			},
			() => {
				peakWeightsHP = waveHP.GetCopy()
					.ApplyFilter(new PeakFilter() { SampleRate = sampleRate, DecayAcceleration = peakWeightPeakAccel })
					.GetSuccessiveLRDiffs(avgBarPeakSmpIdxs, avgBarPeakValues, asdSamples, samplesPerBar);
			}
		);
		
		peakWeights = ArrayUtils.Add(
			peakWeightsLP,
			peakWeightsHP
		)
		.NormaliseMagnitude();


		// ============================= GET PHASE TIME =============================

		float barTimeOfMaxPeakWeight = (float)avgBarPeakSmpIdxs[peakWeights.GetIndexOfMax()] / sampleRate;
		DetectedValue = (decimal)MathUtils.Wrap(clipStartTime + barTimeOfMaxPeakWeight, secondsPerBar);
		Certainty = Mathf.InverseLerp(0.5f, 1,  peakWeights.GetCertaintyOfMax());


		// ============================= DEBUG TEXTURES =============================
		yield break;
		debug_textures:

		AddDebugTex().DrawWave(waveOriginal,	1, lineColor: Color.grey,	clearTex: false).Apply();
//		AddDebugTex().DrawWave(waveLP,			1, lineColor: Color.red,	clearTex: false, isRange01: true).DrawWave(waveLPPeaked, lineColor: Color.red,		clearTex: false, doApply: true, channels: 1).Apply();
//		AddDebugTex().DrawWave(waveHP,			1, lineColor: Color.green,	clearTex: false, isRange01: true).DrawWave(waveHPPeaked, lineColor: Color.green,	clearTex: false, doApply: true, channels: 1).Apply();

		Texture2D debugTex = AddDebugTex();
		debugTex.DrawLine(avgBarHP,			new Color(0, 0.4f, 0));
		debugTex.DrawLine(avgBarHPSmoothed,	new Color(0, 0.4f, 0));
		debugTex.DrawLine(avgBarLP,			new Color(0.4f, 0, 0));
		debugTex.DrawLine(avgBarLPSmoothed,	new Color(0.4f, 0, 0));

		// Draw peaks
		for (int i = 0; i < avgBarPeakSmpIdxs.Count; ++i) {
			debugTex.DrawLineV(0, debugTex.height, debugTex.width * avgBarPeakSmpIdxs[i] / (float)samplesPerBar, new Color(0.4f, 0, 0.4f));
			debugTex.DrawLineV(0, debugTex.height, debugTex.width * (avgBarPeakSmpIdxs[i] - peakMinHalfWidthSamples) / (float)samplesPerBar, new Color(0.2f, 0, 0.2f));
			debugTex.DrawLineV(0, debugTex.height, debugTex.width * (avgBarPeakSmpIdxs[i] + peakMinHalfWidthSamples) / (float)samplesPerBar, new Color(0.2f, 0, 0.2f));
			if (peakWeights != null)
				debugTex.DrawLineV(0, debugTex.height * peakWeights[i], debugTex.width * avgBarPeakSmpIdxs[i] / (float)samplesPerBar, Color.magenta);
		}

		// Draw detected/actual phases
		Func<float, float> Time2Phase = time => MathUtils.Wrap(time - clipStartTime, secondsPerBar) / secondsPerBar;
		int a = Mathf.RoundToInt(debugTex.width * Time2Phase((float)DetectedValue));
		debugTex.DrawLineV(0, debugTex.height, a, Color.red, 3);
		if (ActualValue != -1) {
			int b = Mathf.RoundToInt(debugTex.width * Time2Phase((float)ActualValue));
			debugTex.DrawLineV(0, debugTex.height, b == a ? b + 1 : b, Color.green);
		}

		debugTex.Apply();

		yield break;
	}



	public override decimal ActualValue
	{
		get
		{
			if (Track == null || Track.Clip == null)
				return INVALID_VALUE;

			if (Track.downbeatTime != INVALID_VALUE)
				return Track.downbeatTime;

			decimal phaseTimeActualTmp;
			if (phaseTimes.TryGetValue(Track.Clip.name, out phaseTimeActualTmp))
				return phaseTimeActualTmp;

			return INVALID_VALUE;
		}
	}


	public override decimal GetDifferenceFromActualValue(decimal v)
	{
		return (decimal)MathUtils.GetCyclicDifference((float)ActualValue, (float)v, AudioUtils.SecondsPerBar(Track.bpm));
	}


	public override bool CloseEnough
	{
		get { return Math.Abs(GetDifferenceFromActualValue(DetectedValue)) < 0.06m; }
	}



	private readonly Dictionary<string, decimal> phaseTimes = new Dictionary<string, decimal>()
	{
		{ "174.000 - Deal With It",		0.065m },
		{ "126.000 - Pjanoo",			0.045m },
		{ "172.000 - Shellshock",		0.23m },
		{ "100.000 - Stand Up",			0.51m },
		{ "108.000 - FrainBreeze",		0.03m },
		{ "119.983 - Weekends",			0.027m },
		{ "125.000 - BBBFF",			0.03m },
		{ "174.000 - Star Guitar",		0.07m },
		{ "171.306 - U R Mine",			0.035m },
		{ "174.000 - Tear You Down",	0.09m },
		{ "128.000 - World Of Fantasy",	0.08m },
		{ "174.992 - General Hospital",	0.06m },
		{ "128.000 - Iceladen",			0.05m },
		{ "129.981 - With You Friends",	0.03m },
	};

}
