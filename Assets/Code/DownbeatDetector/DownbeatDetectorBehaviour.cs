﻿using System;
using UnityEngine;


public class DownbeatDetectorBehaviour : DetectorBehaviour<DownbeatDetector>
{

	protected override void OnGUI()
	{
		base.OnGUI();

		if (detectors.Count == 0)
			return;

		Rect r = new Rect(Screen.width - 100, 0, 100, 20);

		DownbeatDetector detector = detectors[detectors.Count - 1];
		float normMouseX = Input.mousePosition.x / Screen.width;
		float timeAtMouseX = Mathf.Lerp(detector.clipStartTime, detector.clipEndTime, normMouseX);
		int sampleAtMouseX = (int)(timeAtMouseX * DownbeatDetector.sampleRate);
		float secondsPerBar = AudioUtils.SecondsPerBar(detector.Track.bpm);
		float timeInBar = MathUtils.Wrap(audioSource.time - detector.clipStartTime, secondsPerBar);
		Func<float, float> Time2ScreenX = time => Screen.width * MathUtils.InverseLerpUnclamped(detector.clipStartTime, detector.clipEndTime, time);

		// Draw time at mouse pos
		GUI.Label(r, timeAtMouseX.ToString());
		r.y += r.height;
		GUI.Label(r, (timeAtMouseX - detector.clipStartTime).ToString());

		// Click to play
		if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
			audioSource.time = timeAtMouseX;

		// Draw clip time
		r.y += r.height;
		GUI.Label(r, audioSource.time.ToString("0.00"));

		// Draw clip playhead
		float a = Screen.height * 2/3f;
		GUIUtils.DrawRect(
			new Rect(Time2ScreenX(audioSource.time), 0, 1, a)
		);


		// Draw bar playhead
		GUIUtils.DrawRect(
			new Rect(
				Screen.width * (timeInBar / secondsPerBar), a,
				1, Screen.height - a
			)
		);
	}
}
