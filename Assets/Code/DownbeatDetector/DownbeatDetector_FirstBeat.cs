﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DownbeatDetector_FirstBeat : Detector
{
	public float clipStartTime = .2f;
	public float clipEndTime = float.MaxValue;
	public const int sampleRate = 2000;
	private readonly Func<float, int> Time2Samples = time => (int)(time * sampleRate);

	const int	filterOrder = 3;
	const float lpCutoff = 100;
	const float peakHold = 0;
	const float peakAccel = 10f;
	const float peakAdjustmentTime = -0.05f;
	
	
	protected override IEnumerator DetectAbstract()
	{
		clipEndTime = Mathf.Min(clipEndTime, Track.Clip.length);
		float clipLengthTime = clipEndTime - clipStartTime;
		int clipLengthSamples = Time2Samples(clipLengthTime);
		int samplesPerBar = AudioUtils.SamplesInBars(1, Track.bpm, sampleRate);
		float secondsPerBar = AudioUtils.SecondsPerBar(Track.bpm);


		// =============================== GET WAVES ===============================

		float[] waveOriginal = new float[clipLengthSamples];
		Track.Clip.GetWaveArray(ref waveOriginal, sampleRate, clipStartTime, clipEndTime);

		float[] waveLP =
			waveOriginal.GetCopy()
			.ApplyFilter(new LowPassFilter() { SampleRate = sampleRate, Order = filterOrder, CutoffFreq = lpCutoff })
			.Abs()
			.Smooth(Time2Samples(0.05f), 3);
			

		// ==========================================================================

		float[] lrDiffs =
			waveLP.GetLRDiffs(Time2Samples(AudioUtils.SecondsPerBar(Track.bpm) * 4))
			.ClampAbove(0)
			.NormaliseMagnitude();

		float[] lrDiffsGradients =
			lrDiffs.GetGradients()
			.Multiply(sampleRate);

		// Find peaks in LR diffs array
		float peakTimeHalfWidthTime = 1;
		int peakTimeHalfWidthSamples  = Time2Samples(peakTimeHalfWidthTime);
		float peakHeight = 0.01f;
		float[] peakTimes = new float[clipLengthSamples];
		float[] peakTimesBar = new float[samplesPerBar];
		for (int i = 0; i < lrDiffs.Length; ++i)
			if (lrDiffs.IsPeak(i, peakTimeHalfWidthSamples, peakHeight) &&
				lrDiffs.IsPeak(i, height: float.Epsilon))
			{
				int i2 = i;//lrDiffsGradients.GetIndexOfMin(i, i + Time2Samples(0.4f));
				peakTimes[i2] = 1;
				peakTimesBar[i2 % samplesPerBar] += 1;//lrDiffs[i];
			}
		peakTimesBar.NormaliseMagnitude();
		float[] peakTimesBarSmoothed = peakTimesBar.GetCopy().SmoothCyclic(Time2Samples(0.05f), 3);
		peakTimesBarSmoothed.NormaliseMagnitude();


		// ============================== DETECT VALUE ==============================
		int indexOfMax = peakTimesBarSmoothed.GetIndexOfMax();

		DetectedValue = (decimal) (
			(
				clipStartTime
				+ indexOfMax / (float)sampleRate
				+ peakAdjustmentTime
			)
			% secondsPerBar
		);

		// ============================= DEBUG TEXTURES =============================

		AddDebugTex().DrawWave(waveOriginal, 1, lineColor: Color.grey).Apply();

		var texWhole = AddDebugTex();
		texWhole.DrawWave(waveLP,			1, isRange01: true,		lineColor: new Color(0.5f, 0, 0));
		//texWhole.DrawWave(waveLPPeaked,	1, isRange01: true,	lineColor: Color.red);
		texWhole.DrawWave(peakTimes,		1, isRange01: true,	lineColor: new Color(0.5f, 0.5f, 0));
		texWhole.DrawWave(lrDiffsGradients,	1, isRange01: false,	lineColor: Color.blue);
		texWhole.DrawLineH(texWhole.height * 0.5f, Color.blue, 3);
		texWhole.DrawWave(lrDiffs,			1, isRange01: true,		lineColor: Color.yellow);
		
		var texBar = AddDebugTex();
		texBar.DrawWave(peakTimesBar,			1, isRange01: true, lineColor: new Color(0.5f, 0.5f, 0));
		texBar.DrawWave(peakTimesBarSmoothed,	1, isRange01: true, lineColor: Color.yellow);

		// Draw detected/actual phases
		Func<float, int> Time2BarTexX = time => (int)(texBar.width * MathUtils.Wrap01(((time - clipStartTime) / secondsPerBar)));
		int detectedValueDebugX = Time2BarTexX((float)DetectedValue);
		texBar.DrawLineV(detectedValueDebugX, Color.red);
		if (ActualValue != -1) {
			int actualValueDebugX = Time2BarTexX((float)ActualValue);
			texBar.DrawLineV(actualValueDebugX == detectedValueDebugX ? actualValueDebugX + 1 : actualValueDebugX, Color.green);
		}

		texBar.Apply();
		texWhole.Apply();
		
		yield break;
	}



	public override decimal ActualValue
	{
		get
		{
			if (Track == null || Track.Clip == null)
				return INVALID_VALUE;

			if (Track.downbeatTime != INVALID_VALUE)
				return Track.downbeatTime;

			decimal phaseTimeActualTmp;
			if (phaseTimes.TryGetValue(Track.Clip.name, out phaseTimeActualTmp))
				return phaseTimeActualTmp;

			return INVALID_VALUE;
		}
	}


	public override decimal GetDifferenceFromActualValue(decimal v)
	{
		return (decimal)MathUtils.GetCyclicDifference((float)ActualValue, (float)v, AudioUtils.SecondsPerBar(Track.bpm));
	}


	public override bool CloseEnough
	{
		get
		{
			decimal diff = GetDifferenceFromActualValue(DetectedValue);
			return diff > -0.06m && diff < 0.05m;
		}
	}



	private readonly Dictionary<string, decimal> phaseTimes = new Dictionary<string, decimal>()
	{
		{ "174.000 - Deal With It",		0.065m },
		{ "126.000 - Pjanoo",			0.045m },
		{ "172.000 - Shellshock",		0.23m },
		{ "100.000 - Stand Up",			0.51m },
		{ "108.000 - FrainBreeze",		0.03m },
		{ "119.983 - Weekends",			0.027m },
		{ "125.000 - BBBFF",			0.03m },
		{ "174.000 - Star Guitar",		0.07m },
		{ "171.306 - U R Mine",			0.035m },
		{ "174.000 - Tear You Down",	0.09m },
		{ "128.000 - World Of Fantasy",	0.08m },
		{ "174.992 - General Hospital",	0.06m },
		{ "128.000 - Iceladen",			0.05m },
		{ "129.981 - With You Friends",	0.03m },
	};

}
