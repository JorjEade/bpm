﻿using UnityEngine;


public class HighPassFilter : AudioFilter
{

	private float cutoffFreq = float.MaxValue;
	private float[] values = new float[10];
	private float[] valuesPrev = new float[10];
	private float alpha = 1;
	private int order = 1;


	public override void Reset()
	{
		values.SetZero();
		valuesPrev.SetZero();
	}


	public override float Update(float input)
	{
		valuesPrev[0] = values[0];
		values[0] = input;

		for (int o = 1; o <= order; ++o)
		{
			valuesPrev[o] = values[o];
			values[o] = values[o] * alpha + (values[o - 1] - valuesPrev[o - 1]);
		}

		return Value;
	}


	protected override void OnSampleRateChanged()
	{
		alpha = AudioUtils.GetCutoffAlphaLowPass(SampleRate, CutoffFreq);
	}


	public int Order
	{
		get { return order; }

		set
		{
			order = value;
			ArrayUtils.EnsureArrayLengthAtLeast(ref values, order + 1);
			ArrayUtils.EnsureArrayLengthAtLeast(ref valuesPrev, order + 1);
		}
	}


	public override float Value
	{
		get { return values[order]; }
	}


	public float CutoffFreq
	{
		get { return cutoffFreq; }

		set
		{
			cutoffFreq = value;
			alpha = AudioUtils.GetCutoffAlphaHighPass(SampleRate, cutoffFreq);
		}
	}

}