﻿using UnityEngine;

public class OscillatorFilter : AudioFilter
{

	public float Damping = 1;

	private float freq;
	private float[] values = new float[10];
	private float[] velocities = new float[10];
	private float a;
	private int order = 1;


	public override void Reset()
	{
		values.SetZero();
		velocities.SetZero();
	}


	public override float Update(float input)
	{
		values[0] = input;

		for (int o = 1; o <= order; ++o)
		{
			float acc = (values[o - 1] - values[o]) * a;
			velocities[o] += (acc - velocities[o] * Damping) * DeltaTime;
			values[o] += velocities[o] * DeltaTime;
		}

		return Value;
	}


	public int Order
	{
		get { return order; }

		set
		{
			order = value;
			ArrayUtils.EnsureArrayLengthAtLeast(ref values, order + 1);
			ArrayUtils.EnsureArrayLengthAtLeast(ref velocities, order + 1);
		}
	}


	public override float Value
	{
		get { return values[order]; }
	}


	public float Freq
	{
		get { return freq; }

		set
		{
			freq = value;
			a = MathUtils.Square(freq * MathUtils.TAU);
		}
	}

}
