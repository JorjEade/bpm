﻿using UnityEngine;


public class LowPassFilter : AudioFilter
{

	private float cutoffFreq = float.MaxValue;
	private float[] easedValues = new float[10];
	private float alpha = 1;
	private int order = 1;


	public override void Reset()
	{
		easedValues.SetZero();
	}


	public override float Update(float input)
	{
		easedValues[0] = input;

		for (int o = 1; o <= order; ++o)
		{
			easedValues[o] += (easedValues[o - 1] - easedValues[o]) * alpha;
		}

		return Value;
	}


	protected override void OnSampleRateChanged()
	{
		alpha = AudioUtils.GetCutoffAlphaLowPass(SampleRate, CutoffFreq);
	}


	public int Order
	{
		get { return order; }

		set
		{
			order = value;
			ArrayUtils.EnsureArrayLengthAtLeast(ref easedValues, order + 1);
		}
	}


	public override float Value
	{
		get { return easedValues[order]; }
	}


	public float CutoffFreq
	{
		get { return cutoffFreq; }

		set
		{
			cutoffFreq = value;
			alpha = AudioUtils.GetCutoffAlphaLowPass(SampleRate, cutoffFreq);
		}
	}

}