﻿public abstract class AudioFilter
{
	public abstract float Update(float input);
	public abstract void Reset();
	public abstract float Value { get; }
	protected virtual void OnSampleRateChanged() { }

	private int sampleRate = 44100;
	private float deltaTime = 1 / 44100f;


	public int SampleRate
	{
		get { return sampleRate; }

		set
		{
			sampleRate = value;
			deltaTime = 1f / sampleRate;
			OnSampleRateChanged();
		}
	}


	protected float DeltaTime
	{
		get { return deltaTime; }
	}


	public void Apply(float[] target)
	{
		Reset();

		for (int i = 0; i < target.Length; ++i)
			target[i] = Update(target[i]);
	}


	public void ApplyReverse(float[] target)
	{
		Reset();

		for (int i = target.Length - 1; i >= 0; --i)
			target[i] = Update(target[i]);
	}
}
