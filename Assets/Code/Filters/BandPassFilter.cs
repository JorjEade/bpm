﻿using UnityEngine;


public class BandPassFilter : AudioFilter
{

	private readonly LowPassFilter filterLP = new LowPassFilter();
	private readonly HighPassFilter filterHP = new HighPassFilter();



	public override void Reset()
	{
		filterLP.Reset();
		filterHP.Reset();
	}


	public override float Update(float input)
	{
		filterLP.Update(input);
		filterHP.Update(filterLP.Value);

		return Value;
	}


	protected override void OnSampleRateChanged()
	{
		filterLP.SampleRate = SampleRate;
		filterHP.SampleRate = SampleRate;
	}


	public int Order
	{
		get { return filterLP.Order; }

		set
		{
			filterLP.Order = value;
			filterHP.Order = value;
		}
	}


	public override float Value
	{
		get { return filterHP.Value; }
	}


	public float CutoffFreqLow
	{
		get { return filterHP.CutoffFreq; }

		set { filterHP.CutoffFreq = value; }
	}


	public float CutoffFreqHigh
	{
		get { return filterLP.CutoffFreq; }

		set { filterLP.CutoffFreq = value; }
	}

}