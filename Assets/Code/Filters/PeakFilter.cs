﻿using UnityEngine;


public class PeakFilter : AudioFilter
{

	public float DecayAcceleration;
	public float HoldTime;

	private float velocity;
	private float peak;
	private float holdTimer;


	public override float Update(float input)
	{
		if (input * input > peak * peak)
		{
			peak = Mathf.Abs(input);
			velocity = 0;
			holdTimer = HoldTime;
		}
		else if (holdTimer > 0)
		{
			holdTimer -= DeltaTime;
		}
		else
		{
			velocity -= DecayAcceleration * DeltaTime;
			peak += velocity * DeltaTime;

			if (peak < 0) {
				peak = 0;
				velocity = 0;
			}
		}

		return peak;
	}


	public override void Reset()
	{
		velocity = 0;
		peak = 0;
		holdTimer = 0;
	}


	public override float Value
	{
		get { return peak; }
	}


}
