﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DetectorBehaviour<TDetector> : MonoBehaviour where TDetector : Detector, new()
{
	
	private const int barToPlayFrom = 30;
	public List<AudioClip> clips = new List<AudioClip>();
	public bool playClipOnDetected;

	protected List<TDetector> detectors = new List<TDetector>();
	protected AudioSource audioSource;



	
	private IEnumerator Start()
	{
		Application.runInBackground = true;

		audioSource = gameObject.GetComponent<AudioSource>() ?? gameObject.AddComponent<AudioSource>();

		for (int i = 0; i < clips.Count; ++i)
		{
			yield return _Detect(new Track() { Clip = clips[i] });
			yield return null;
		}
	}


	protected virtual IEnumerator OnDetected(TDetector detector)
	{
		yield break;
	}


	
	public void Detect(Track track)
	{
		enabled = true;
		StartCoroutine(_Detect(track));
	}



	public IEnumerator _Detect(Track track)
	{
		TDetector detector = new TDetector();
		detectors.Add(detector);

		yield return detector.Detect(track);

		audioSource.Stop();

		if (playClipOnDetected
			//&& detector.DetectedValue != Detector.INVALID_VALUE
			)
		{
			audioSource.clip = detector.Track.Clip;
			audioSource.time = detector.DetectedValue == -1
				? 0
				: (float)detector.DetectedValue + AudioUtils.SecondsPerBar(detector.Track.bpm) * (barToPlayFrom % (int)detector.Track.Clip.GetNumBars(detector.Track.bpm));
			audioSource.Play();
		}

		yield return OnDetected(detector);
	}



	public void Clear()
	{
		detectors.ForEach(d => d.Track.UnloadClip());
		detectors.Clear();
	}



	protected virtual void OnGUI()
	{
		for (int i = 0; i < detectors.Count; ++i)
		{
			TDetector detector = detectors[i];

			if (detector == null)
				continue;

			Rect r = new Rect(0, Screen.height * (i / (float)detectors.Count), Screen.width, Screen.height / (float)detectors.Count);

			for (int debugTexIndex = 0; debugTexIndex < detector.debugTextures.Count; ++debugTexIndex)
			{
				float debugTexHeight = r.height / detector.debugTextures.Count;
				Rect debugTexRect = new Rect(r.x, r.y + debugTexHeight * debugTexIndex, r.width, debugTexHeight);
				GUI.DrawTexture(debugTexRect, detector.debugTextures[debugTexIndex]);
			}

			GUI.Label(
				r,
				string.Format(
					"{0} [{1}] - <color={2}>{3}</color>",
					detector.Track.name,
					detector.ActualValue,
					detector.DetectedColor,
					detector.DetectedValue
				)
			);
		}
	}

}
